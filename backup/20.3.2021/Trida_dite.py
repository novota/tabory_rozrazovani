import numpy as np
from datetime import date

from Hlavni_kod.Help_functions import mam_pridat_dite, verify_friend_codes_quantity


class Seznam_deti:

	def __init__(self, seznam_deti=[]):
		self.seznam_deti = seznam_deti

	def __iadd__(self, other):
		assert isinstance(other, Dite)
		self.seznam_deti.append(other)
		return self

	def __isub__(self, other):
		assert isinstance(other, Dite)
		self.seznam_deti.remove(other)
		return self

	def __add__(self, other):
		assert isinstance(other, Dite)
		self.seznam_deti.append(other)
		return self

	def __str__(self):
		a = 'seznam deti: \n'
		for element in self.seznam_deti:
			a += str(element)
		return a

	def delete_kid_with_friends(self, kid):
		assert isinstance(kid, Dite)
		kamarad1 = kid.kamarad1
		kamarad2 = kid.kamarad2
		try:
			self.seznam_deti.remove(kid)
		except ValueError:
			pass
		try:
			if isinstance(kamarad1, Dite):
				self.seznam_deti.remove(kamarad1)
		except ValueError:
			pass
		try:
			if isinstance(kamarad2, Dite):
				self.seznam_deti.remove(kamarad2)
		except ValueError:
			pass

		return self

	def inicializace_seznamu_deti(self, vstup, offset):
		input_v = vypocitat_vek(vstup, offset)  # vytvoreny seznam deti
		if isinstance(input_v[0], Dite):
			self.seznam_deti = input_v
			return self
		else:
			return input_v

	def odebrat_oddily(self):
		for dite in self.seznam_deti:
			dite.oddil = -1
		return self

	# finds the best margin, self.seznam_deti must be sorted in descending order
	def find_margin(self, pocet_oddilu):
		start = 0
		end = start + pocet_oddilu
		diff = 0
		count = len(self.seznam_deti) - end
		while end < len(self.seznam_deti):
			diff += self.seznam_deti[start].age - self.seznam_deti[end].age
			start += 1
			end += 1
		return diff / count

	# seznam deti serazenych podle veku od nejstarsiho po nejmladsi, bere jen kamarady stejneho pohlavi protoze jde o razeni do chatek
	def get_podseznam(self, spodni_hranice):
		podseznam = []
		if len(self.seznam_deti) == 0:
			return podseznam
		index = 0
		while len(self.seznam_deti) > 0 and self.seznam_deti[0].age >= spodni_hranice:
			dite = self.seznam_deti[index]
			# pripadne pridat kamarady pokud spodni hranice >= 16 nesmime pridat dite s kamarady
			kamarad1 = dite.kamarad1
			kamarad2 = dite.kamarad2
			if spodni_hranice >= 16 and kamarad1 == -1 and kamarad2 == -1:
				podseznam.append(dite)
				self.seznam_deti.remove(dite)
			elif spodni_hranice >= 16 and (not kamarad1 == -1 or not kamarad2 == -1):
				index += 1
			if spodni_hranice < 16:
				podseznam.append(dite)
				self.seznam_deti.remove(dite)
				if not kamarad1 == -1 and dite.gender == kamarad1.gender:
					podseznam.append(kamarad1)
					self.seznam_deti.remove(kamarad1)  # we must remove it from the list so that we dont use it later
				if not kamarad2 == -1 and dite.gender == kamarad2.gender:
					podseznam.append(kamarad2)
					self.seznam_deti.remove(kamarad2)

		return podseznam

	def podseznam_nejmladsich_holek(self, pocet_mist_s_omezenim):
		# seradi si seznam od nejmladsich po nejstarsi
		podseznam = []
		self.seznam_deti.sort(key=lambda x: x.age)
		index = 0
		while pocet_mist_s_omezenim > 0:
			dite = self.seznam_deti[index]
			kamarad1 = dite.kamarad1
			kamarad2 = dite.kamarad2
			mnozstvi_deti = 1
			if kamarad1 == -1 or (kamarad1.gender == "female" and abs(kamarad1.age - dite.age) < 0.7 or kamarad1.gender == "male"):
				if not kamarad1 == -1 and kamarad1.gender == "female":
					mnozstvi_deti += 1
				if kamarad2 == -1 or (kamarad2.gender == "female" and abs(kamarad2.age - dite.age) < 0.7 or kamarad2.gender == "male"):
					if not kamarad2 == -1 and kamarad2.gender == "female":
						mnozstvi_deti += 1
					if mnozstvi_deti <= pocet_mist_s_omezenim:
						pocet_mist_s_omezenim -= mnozstvi_deti
						podseznam.append(dite)
						self.seznam_deti.remove(dite)
						if not kamarad1 == -1 and kamarad1.gender == "female":
							podseznam.append(kamarad1)
							self.seznam_deti.remove(kamarad1)
						if not kamarad2 == -1 and kamarad1.gender == "female":
							podseznam.append(kamarad2)
							self.seznam_deti.remove(kamarad2)
					else:
						index += 1
				else:
					index += 1
			else:
				index += 1
		return podseznam

	# vytvori 2d tabulku pripravenou k transformaci pro excel
	def vystup_2D_pro_excel(self):
		seznam = [["" for x in range(9)] for y in range(len(self.seznam_deti) + 1)]  # pocet radku je o jeden vetsi, protoze pridavame pojmenovani

		# prvni radek
		seznam[0][0] = "O"
		seznam[0][1] = "U"
		seznam[0][2] = "Kód"
		seznam[0][3] = "S"
		seznam[0][4] = "Jméno"
		seznam[0][5] = "R"
		seznam[0][6] = "M"
		seznam[0][7] = "D"
		seznam[0][8] = "Město"

		# zbytek radku doplnit automaticky smyckou, pocet radku je + 1
		for index, dite in enumerate(self.seznam_deti):
			seznam[index + 1][0] = dite.oddil.cislo_oddilu
			if dite.f_code == -1:
				seznam[index + 1][2] = ""
			else:
				seznam[index + 1][2] = dite.f_code
			seznam[index + 1][3] = dite.gender
			seznam[index + 1][4] = dite.name
			seznam[index + 1][5] = dite.r
			seznam[index + 1][6] = dite.m
			seznam[index + 1][7] = dite.d
			seznam[index + 1][8] = dite.Mesto

			if dite.chatka is not None:
				seznam[index + 1][1] = dite.chatka.cislo_chatky
			else:
				seznam[index + 1][1] = ""

		# tot vse
		return seznam

	# returns True if any kid dosnt have oddil

	def nezarazene_deti(self):
		for dite in self.seznam_deti:
			if dite.oddil == -1:
				return True
		return False

	def seradit_sestupne(self):
		index = 1
		while index < len(self.seznam_deti):
			j = index - 1
			while j >= 0:
				if self.seznam_deti[j + 1].age > self.seznam_deti[j].age:
					temp = self.seznam_deti[j]
					self.seznam_deti[j] = self.seznam_deti[j + 1]
					self.seznam_deti[j + 1] = temp
					j -= 1
				else:
					break
			index += 1
		return self

	def vyselektovat_deti_s_kamaradem(self):
		result = []
		for index, element in enumerate(self.seznam_deti):
			if not element.kamarad1 == -1:
				result.append(element)
		return Seznam_deti(result)

	def pocet_deti(self):
		return len(self.seznam_deti)

	def prumerny_vek(self):
		counter = 0
		sum = 0
		for element in self.seznam_deti:
			sum += element.age
			counter += 1
		return sum / counter

	def seznam_kluku(self):
		seznam_boys = [x for x in self.seznam_deti if x.gender == 'H']
		seznam_boys = Seznam_deti(seznam_boys)
		return seznam_boys

	def seznam_holek(self):
		seznam_girls = [x for x in self.seznam_deti if x.gender == 'D']
		seznam_girls = Seznam_deti(seznam_girls)
		return seznam_girls

	def find_no_house_kid(self):
		for kid in self.seznam_deti:
			if kid.chatka is None:
				return kid
		return None

	def find_no_oddil_kid(self, margin, kapacita, oddil=None):
		for kid in self.seznam_deti:
			if kid.oddil == -1 and kid.pocet_kamaradu() + 1 <= kapacita:
				if oddil == None:
					return True
				pridat, duvod = mam_pridat_dite(oddil, kid, margin)
				if pridat:
					return kid
				else:
					return duvod
		return None

# nahradi parametr kamarad z cisla na primy odkaz kamarada, u ostatnich nech -1


#  def priradit_kamarady(self):
#     for index in range (len(self.seznam_deti)): #najdi prvni decko s kamaradem
#          if self.seznam_deti[index].kamarad != -1:
#              kamarad_cislo = self.seznam_deti[index].kamarad
#             index2 = index + 1
#             seznam_kamaradu = [self.seznam_deti[index]]
#             while index2 < len(self.seznam_deti):  #najdi mu zbytek kamaradu a vsechny uloz do listu
#                  if self.seznam_deti[index2].kamarad == kamarad_cislo:
#                      seznam_kamaradu.append(self.seznam_deti[index2])
#                 index2 += 1


class Dite:

	def __init__(self, name='none', age=-1, gender='none', rok=-1, month=-1, day=-1, friends_code=-1, oddil=-1, kamarad1=-1, kamarad2=-1):
		self.name = name
		self.age = age
		self.gender = gender
		self.oddil = oddil
		self.chatka = None
		self.kamarad1 = kamarad1
		self.kamarad2 = kamarad2
		self.r = rok
		self.m = month
		self.d = day
		self.f_code = friends_code
		self.Mesto = ""
		self.P = ""
		self.Platba = ""
		self.Doplatek = ""
		self.K = ""
		self.W = ""
		self.Kamarad = ""
		self.Pozn_D = ""
		self.Jmeno_R1 = ""
		self.Tel_R1 = ""
		self.Jmeno_R2 = ""
		self.Tel_R2 = ""
		self.poukazy = ""

	def __str__(self):
		if self.oddil == -1:
			result = '{:s} vek {:f} pohlavi {:s} oddil c. {:d} Kod {:d}'.format(self.name, self.age, self.gender, -1, int(self.f_code))
		else:
			result = '{:s} vek {:f} pohlavi {:s} oddil c. {:d} Kod {:d}'.format(self.name, self.age, self.gender, self.oddil.cislo_oddilu, int(self.f_code))
		if self.chatka is not None:
			result += ' chatka c. {:s}'.format(str(self.chatka.cislo_chatky))
		else:
			result += ' bez chatky'
		result += '\n'
		return result

	def pocet_kamaradu(self):
		return int(self.kamarad1 != -1) + int(self.kamarad2 != -1)

	def pocet_kamaradu_stejneho_pohlavi(self):
		result = 1
		if self.kamarad1 != -1 and self.kamarad1.gender == self.gender:
			result += 1
		if self.kamarad2 != -1 and self.kamarad2.gender == self.gender:
			result += 1
		return result

	# returns the oldest friends age who has the same gender
	def vek_nejstersiho_z_kamaradu(self):
		result = self.age
		if self.kamarad1 != -1 and self.kamarad1.age > result and self.kamarad1.gender == self.gender:
			result = self.kamarad1.age
		if self.kamarad2 != -1 and self.kamarad2.age > result and self.kamarad2.gender == self.gender:
			result = self.kamarad2.age
		return result

	def cislo_oddilu(self):
		return int(self.cislo_oddilu())


# funkce ulozi kazdemu objektu dite vsechny jeho kamarady , maximalne dva
def save_friends(seznam_kodu, seznam_deti):
	# projet seznam kodu radek po radku
	for indexR in range(len(seznam_kodu)):
		if seznam_kodu[indexR][0] == 0:  # end loop if there are no more friends codes in the position 0
			break
		# inicializace tri moznych kamaradu
		kamarad1 = None
		kamarad2 = None
		kamarad3 = None
		# ulozit vsechny potencialni kamarady, kamarad 3 muze byt 0
		kamarad1 = seznam_deti[int(seznam_kodu[indexR][1])]

		if not int(seznam_kodu[indexR][2]) == -1:
			kamarad2 = seznam_deti[int(seznam_kodu[indexR][2])]
			kamarad1.kamarad1 = kamarad2
			kamarad2.kamarad1 = kamarad1
		if not int(seznam_kodu[indexR][3]) == -1:
			kamarad3 = seznam_deti[int(seznam_kodu[indexR][3])]
			kamarad1.kamarad2 = kamarad3
			kamarad2.kamarad2 = kamarad3
			kamarad3.kamarad1 = kamarad1
			kamarad3.kamarad2 = kamarad2


# ve skutecnosti tato funkce dela komplet inicializaci seznamu deti se vsim co k tomu patri
def vypocitat_vek(input, offset):  # offset je pocet dni od posledniho tabora ke kteremu se data vztahuji, testovaci duvody
	shape = input.shape  # tuple containing shape, rows, columns
	seznam = []  # seznam deti
	seznam_kodu = np.zeros(shape=(shape[0], 4)) - 1
	# np.zeros(shape=(shape[0], shape[1] - 2), dtype="str")

	# kontrola poctu kodu
	list_of_codes = input[:, 5]
	f_codes, kolikrat = verify_friend_codes_quantity(list_of_codes)
	if len(f_codes) != 0:
		result = [f_codes, kolikrat]
		return result

	# projet seznam a v kazdem radku projet vsechny sloupce a vyzobat na vsech pozicich data a ulozit je
	for indexr, row in enumerate(input):
		# if there is no more entries, break
		if input[indexr, 0] == -1:
			break
		jmeno = "none"
		rok = -1
		mesic = -1
		den = -1
		pohlavi = "none"
		friends_code = 0
		# projet radek
		for indexc, element in enumerate(row):
			if indexc == 0:  # this is jmeno
				jmeno = input[indexr, indexc]
			elif indexc == 1:  # this is the year
				rok = input[indexr, indexc]
			elif indexc == 2:  # this is the month
				mesic = input[indexr, indexc]
			elif indexc == 3:  # this is the day
				den = input[indexr, indexc]
			elif indexc == 4:  # this is the gender
				pohlavi = input[indexr, indexc]
			elif indexc == 5:  # this is the friends code
				friends_code = input[indexr, indexc]
			elif indexc == 6:  # this is the kids city
				city = input[indexr, indexc]

		# vypocitat vek, zjistit pohlavi a priradit kod kamarada
		today = date.today()
		rozdil_rok = today.year - rok
		rozdil_mesic = today.month - mesic
		rozdil_den = today.day - den
		# prepocitat na dny
		rozdil_rok = rozdil_rok * 365
		rozdil_mesic = rozdil_mesic * 30  # nebude to uplne presny ale to je jedno
		vek = rozdil_rok + rozdil_mesic + rozdil_den - offset  # vek ve dnech
		vek = vek / 365  # prepocet na roky
		# ulozit friends code, projdi seznam kodu a najdi prazdne misto nebo najdi matching friends code
		# nahlasit pokud nekdo 16ti letej ma kamarada, nepridame ho tam
		if vek >= 16 and not -1 == friends_code:
			print("16 ti lety tabronik ma zapsaneho kamarada, to nejde, kamarad nebyl pridan!!")
			friends_code = - 1
		if vek < 16 and not -1 == friends_code:
			didWeSaveIt = False  # jen pro jistotu jestli nam nedoslo misto v seznamu
			for indexRow in range(len(seznam_kodu)):
				if friends_code == seznam_kodu[indexRow][0]:  # if code is already in the list save it in free column and end loop
					for indexCol in range(4):
						if seznam_kodu[indexRow][indexCol] == -1:
							seznam_kodu[indexRow][indexCol] = indexr  # save the index of the friend here and end loop
							didWeSaveIt = True
							break
					break  # end loop (indexRow loop)
				elif seznam_kodu[indexRow][0] == -1:  # nenasli jsme matching friends code tak udelame novej entry a ukoncime loop
					seznam_kodu[indexRow][0] = friends_code
					seznam_kodu[indexRow][1] = indexr
					didWeSaveIt = True
					break
			if not didWeSaveIt:
				print("hups")

			assert didWeSaveIt, "we didnt save the friends code, I think we run out of space in the code list"

		# vytvorit dite a ulozit ho do seznamu
		akid = Dite(jmeno, vek, pohlavi, rok, mesic, den, friends_code)
		akid.Mesto = city
		seznam.append(akid)
	# ted ulozim kamarady k sobe dle seznamu kodu
	save_friends(seznam_kodu, seznam)  # poslu seznam deti a seznam kodu do funkce, ktera priradi objekty kamaradu k sobe
	return seznam


'''
p = [Dite('Pepa', 'Smetana1', 11, 'female'), Dite('Pepa', 'Smetana2', 11, 'female'), Dite('Pepa', 'Smetana3', 11, 'female'),
     Dite('Pepa', 'Smetana4', 12, 'female'), Dite('Pepa', 'Smetana5', 12, 'female'), Dite('Pepa', 'Smetana6', 12, 'male'),
     Dite('Pepa', 'Smetana7', 13, 'male'),
     Dite('Pepa', 'Smetana8', 13, 'female', kamarad = 2), Dite('Pepa', 'Smetana9', 13, 'male'), Dite('Pepa', 'Smetana10', 14, 'female'),
     Dite('Pepa', 'Smetana11', 14, 'female', kamarad = 4), Dite('Pepa', 'Smetana12', 14, 'female'),
     Dite('Pepa', 'Smetana13', 15, 'male'), Dite('Pepa', 'Smetana14', 15, 'male'),
     Dite('Pepa', 'Smetana15', 11, 'female'), Dite('Pepa', 'Smetana16', 15, 'male', kamarad = 1),
     Dite('Pepa', 'Smetana17', 11, 'male',kamarad =  5), Dite('Pepa', 'Smetana18', 11, 'female'),
     Dite('Pepa', 'Smetana19', 12, 'female'), Dite('Pepa', 'Smetana20', 13, 'female', kamarad = 2),
     Dite('Pepa', 'Smetana21', 15, 'male'), Dite('Pepa', 'Smetana22', 13, 'female'),
     Dite('Pepa', 'Smetana23', 14, 'male'), Dite('Pepa', 'Smetana24', 15, 'male', kamarad = 1),
     Dite('Pepa', 'Smetana25', 11, 'female'),
     Dite('Pepa', 'Smetana26', 12, 'male', kamarad = 3), Dite('Pepa', 'Smetana27', 12, 'female'),
     Dite('Pepa', 'Smetana28', 13, 'male'), Dite('Pepa', 'Smetana29', 13, 'female', kamarad = 2),
     Dite('Pepa', 'Smetana30', 12, 'male'), Dite('Pepa', 'Smetana31', 12, 'male'), Dite('Pepa', 'Smetana32', 13, 'male'),
     Dite('Pepa', 'Smetana33', 12, 'male'), Dite('Pepa', 'Smetana34', 13, 'female', kamarad = 4),
     Dite('Pepa', 'Smetana35', 14, 'female'), Dite('Pepa', 'Smetana36', 15, 'male'),
     Dite('Pepa', 'Smetana37', 11, 'male', kamarad = 5),
     Dite('Pepa', 'Smetana38', 12, 'male', kamarad = 3), Dite('Pepa', 'Smetana39', 12, 'male'),
     Dite('Pepa', 'Smetana40', 13, 'female'), Dite('Pepa', 'Smetana41', 13, 'female'),
     Dite('Pepa', 'Smetana42', 12, 'female'), Dite('Pepa', 'Smetana43', 15, 'female'), Dite('Pepa', 'Smetana44', 13, 'female')]
    seznam = Seznam_deti()
    seznam.seznam_deti = [x for x in p]
    '''
