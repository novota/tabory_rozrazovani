# -*- coding: utf-8 -*-
"""
Created on Tue Oct 29 12:09:24 2019

@author: petrnovota
"""
import pandas as pd

from Hlavni_kod.Help_functions import add_oddily_to_excel, convert_string_to_month_and_day, get_datetime, get_odd_count, \
	check_single_codes
from Hlavni_kod.Trida_chatka import Seznam_chatek
from Hlavni_kod.Trida_dite import Seznam_deti
from Hlavni_kod.Trida_oddil import Seznam_oddilu
import datetime
import tkinter as tk
from tkinter.filedialog import askopenfilename

# TODO: pridat kontrolu, ze maji maximalne 3 deti stejny friends code

# globalni promene
# offset = pocet dni od tabora od ktereho mam data na testovani, tak aby sedel vek taborniku k datu konani tabora
global file_name, offset
seznam_oddilu = Seznam_oddilu()
global seznam_deti_in_numpy
global tabor_date
global max_kids_oddil, odd_1, odd_2, pocatecni_cislo_odd
max_lidi_v_oddile = -1


# tkinter stuff
def end():
	main.destroy()


def OpenFile():
	global file_name, seznam_deti_in_numpy
	file_name = askopenfilename(
		filetypes=(("excel files", "*.xlsx"), ("All Files", "*.*")),
		title="nacti taborovy soubor"
	)
	# Using try in case user types in unknown file or closes without choosing a file.

	try:
		seznam_xlsx = pd.read_excel(file_name)
		seznam_komplet = seznam_xlsx.loc[:, ('Jméno', 'R', 'M', 'D', 'S', 'Kód', 'Město')]
		seznam_komplet.fillna(-1, inplace=True)  # nahradi nan hodnoty -1kou
		seznam_deti_in_numpy = seznam_komplet.values

		# spocitej kolik je deti v seznamu a zobraz tuto informaci
		(rows, cols) = seznam_deti_in_numpy.shape
		l01['text'] = rows

	except:
		l_info_panel['text'] = 'Nastala chyba pri načítání souboru. Zkontroluj názvy sloupců. Musí být: ' \
		                       'Jméno, R, M, D, S, Kód, Město'
		print("No file exists")


def potvrd_zadani():
	global tabor_date, offset, odd_1, odd_2, max_kids_oddil, pocatecni_cislo_odd
	try:
		index = list_of_dates.curselection()
		text = list_of_dates.get(index)
		list_of_dates.see(index)
	except:
		l_info_panel['text'] = 'Musíš vybrat nějaký termín konání tábora!'
		return
	# get day and month from the string
	day, month = convert_string_to_month_and_day(text)  # day and month are both strings
	try:
		year_str = year.get()
		test = year_str[3]
		# create string of date convertible to datetime object. format
		tabor_date = get_datetime(day, month, year_str)
		text = "Datum tábora: " + tabor_date.date().strftime('%d/%m/%Y')
		l_info_panel['text'] = text
		# compute offset
		# TODO mozna bysme meli dat vypocet offsetu mimo tento try except blok
		offset = datetime.date.today() - tabor_date.date()
		offset = offset.days.real
	except:
		l_info_panel['text'] = 'Vybral jsi den ' + day + ' a mesíc ' + month + ' ale musíš ještě zadat rok konání tábora ve formátu RRRR!'

	# get the number of oddilu and create a list of oddilu
	try:
		max_kids_oddil = int(max_osob.get())
		pocatecni_cislo_odd = int(starting_nr_odd.get())
		# odd_1 is the number of oddily with capacity max_kids_oddil. odd_2 is the number of oddily with capacity max_kids_oddil - 1
		(rows, cols) = seznam_deti_in_numpy.shape  # how many kids are there in total
		odd_1, odd_2 = get_odd_count(rows, max_kids_oddil)

		l6['text'] = str(odd_2) + ' po ' + str(max_kids_oddil - 1) + ' dětech a ' + str(odd_1) + ' po ' + str(max_kids_oddil) + ' dětech'
		print(seznam_oddilu)
	except:
		l_info_panel['text'] = 'Načti soubor s daty a vyplň max. počet dětí na oddíl a počáteční číslo oddílu'


def vytvor_oddily():
	global seznam_deti_in_numpy, offset, max_kids_oddil, odd_1, odd_2, pocatecni_cislo_odd
	# clear list of codes
	if list_of_codes.size() != 0:
		list_of_codes.delete(0, list_of_codes.size())
	l7['text'] = 'seznam problematických kódů'
	try:
		seznamdeti = Seznam_deti().inicializace_seznamu_deti(seznam_deti_in_numpy, offset)
		# inicializace oddillu
		seznam_oddilu.inicializace_oddilu(max_kids_oddil - 1, odd_2, pocatecni_cislo_odd)
		seznam_oddilu.inicializace_oddilu(max_kids_oddil, odd_1, pocatecni_cislo_odd + odd_2)
		test_value = seznam_oddilu.seznam_oddilu[0]  # iff array has not been initialized yet, this yields error
	except:
		l_info_panel['text'] = 'Nejdříve načti soubor s daty, vyber termín tábora a potvrď tlačítkem OK'
		return
	# make sure there is maximum 3 same friends codes, if not print them and end function with return
	if not isinstance(seznamdeti, Seznam_deti):
		# check if there are code which appear only once and print them
		singl_kody = check_single_codes(seznam_deti_in_numpy)
		if len(singl_kody) > 0:
			for index, code in enumerate(singl_kody):
				text = 'kód ' + str(int(code)) + ' je tu jen jednou'
				list_of_codes.insert('end', text)
			l7['text'] = 'Seznam problematických kódů. Prověř!!'
		friends_code = seznamdeti[0]
		kolikrat = seznamdeti[1]
		if len(friends_code) > 0:
			for index, code in enumerate(friends_code):
				text = 'kód ' + str(int(code)) + ' je tu ' + str(kolikrat[index]) + 'x'
				list_of_codes.insert('end', text)
			l_info_panel['text'] = 'některé kódy jsou v seznamu více než 3x viz. seznam kódů navíc. Oprav a zkus to znovu'
			return
	# kontrola, ze mame dostatek mista pro vsechny pionyry?
	# prirazeni deti do oddilu
	seznam_oddilu.naplnit_oddily_na_kapitana_varianta(seznamdeti)

	for oddil in seznam_oddilu.seznam_oddilu:
		oddil.seznam_deti.sort(key=lambda x: x.age, reverse=True)
	seznam_oddilu.seznam_oddilu.sort(key=lambda x: x.cislo_oddilu)

	# vytvor excel
	# prevod seznamu do pandas data frame
	a1 = pd.DataFrame(seznamdeti.vystup_2D_pro_excel())
	odd = a1.loc[:, 0]
	odd = pd.DataFrame({'seznam': odd.values})

	# for testing purpose create a array with ages of kids and add it to a column in output
	vek = []
	for di in seznamdeti.seznam_deti:
		vek.append(di.age)

	add_oddily_to_excel(odd, file_name, 2, vek)
	# TODO zjisti, jestli existuji kody ktere jsou jen jednou v seznamu a nahlas je jako upozorneni
	l_info_panel['text'] = 'Oddily byly úspěšně vytvořeny!'
	# check if there are code which appear only once and print them
	singl_kody = check_single_codes(seznam_deti_in_numpy)
	if len(singl_kody) > 0:
		for index, code in enumerate(singl_kody):
			text = 'kód ' + str(int(code)) + ' je tu jen jednou'
			list_of_codes.insert('end', text)
		l7['text'] = 'Seznam samostatných kódů. Prověř!!'
		l_info_panel['text'] = 'Oddily byly úspěšně vytvořeny, ale ověř kódy, které jsou jen jednou!'
	print(seznam_oddilu)
	seznam_oddilu.clear()

	return


def vytvor_ubytovani(seznam_deti):
	# serad deti podle veku od nejmensich po nejvetsi a podle oddilu
	seznam_deti.seznam_deti.sort(key=lambda x: (x.oddil.cislo_oddilu, x.age))
	# print(seznam_deti)

	# inicializace a ulozeni radova
	# seznam_Radov = Seznam_chatek("Novy Radov Hornak")
	# seznam_Radov.inicializace_chatek()
	# seznam_Radov.save()
	novy_seznam = Seznam_chatek("Novy_Radov_Hornak.csv").load()
	zkraceny_seznam = novy_seznam.cast_chatek(20)  # tweak tady muzu vybrat podskupinu chatek
	zkraceny_seznam.vytvorit_ubytovani(seznam_deti)


# print(zkraceny_seznam)
# seznam_podseznamu, seznam_rozlozeni = novy_seznam.naplnit_chatky(seznam_deti, 4)
# print(seznam_deti)


# tkinter


main = tk.Tk()
main.title("Tábory rozřazování")
label = tk.Label(main, height='10', width='40', bg='white')

max_osob = tk.StringVar()
starting_nr_odd = tk.StringVar()
total_kids = tk.StringVar()
year = tk.StringVar()

b1 = tk.Button(main, text="end", command=end, height='5', width='20')
b1.grid(row=0, rowspan=2, column=0, sticky='we')
b2 = tk.Button(main, text="open file", command=OpenFile, height='5', width='20')
b2.grid(row=2, rowspan=2, column=0, sticky='we')
b4 = tk.Button(main, text="vytvor oddily", command=vytvor_oddily, height='5', width='20')
b4.grid(row=4, rowspan=2, column=0, sticky='we')
b5 = tk.Button(main, text="OK", command=potvrd_zadani)
b5.grid(row=6, column=2, sticky='we')

l0 = tk.Label(main, text='v seznamu je celkem pionýrů')
l0.grid(row=0, column=1, sticky='we')
l01 = tk.Label(main, text='0')
l01.grid(row=0, column=2, sticky='we')
l1 = tk.Label(main, text='pocet oddílů')
l1.grid(row=1, column=1, sticky='we')
l2 = tk.Label(main, text='max. počet dětí na oddíl')
l2.grid(row=2, column=1, sticky='we')
l3 = tk.Label(main, text='počáteční číslo oddílu')
l3.grid(row=3, column=1, sticky='we')
l_info_panel = tk.Label(main, text='Info panel')
l_info_panel.grid(row=7, column=0, columnspan=4)
l4 = tk.Label(main, text='Rok konání tábora')
l4.grid(row=5, column=1, sticky='we')
l5 = tk.Label(main, text='Vyber termín konání tábora')
l5.grid(row=4, column=1, sticky='we')
l6 = tk.Label(main, text='')
l6.grid(row=1, column=2, sticky='we')
l7 = tk.Label(main, text='seznam kódů navíc')
l7.grid(row=3, column=3, sticky='we')

entry2 = tk.Entry(main, textvariable=max_osob)
entry2.grid(row=2, column=2)
entry3 = tk.Entry(main, textvariable=starting_nr_odd)
entry3.grid(row=3, column=2)
entry4 = tk.Entry(main, textvariable=year)
entry4.grid(row=5, column=2)

# scrollbar widget
choose_date_scroll = tk.Scrollbar(main, orient="horizontal")
list_of_dates = tk.Listbox(main, height=3, yscrollcommand=choose_date_scroll.set)
choose_date_scroll['command'] = list_of_dates.yview
# create elements of the scrollbar widget
list_of_dates.insert('end', '4.7.')
list_of_dates.insert('end', '18.7.')
list_of_dates.insert('end', '25.7.')
list_of_dates.insert('end', '1.8.')
list_of_dates.insert('end', '8.8.')
list_of_dates.insert('end', '15.8.')
list_of_dates.insert('end', '22.8.')
# show result
list_of_dates.grid(row=4, column=2)
choose_date_scroll.grid(row=4, column=2)

# another scrollbar widget
kody_navic = tk.Scrollbar(main, orient="horizontal")
list_of_codes = tk.Listbox(main, height=3, yscrollcommand=kody_navic.set)
kody_navic['command'] = list_of_codes.yview()
# show result
list_of_codes.grid(row=4, column=3)
kody_navic.grid(row=4, column=3)

main.mainloop()

'''
seznam_deti, seznam_oddilu = vytvor_oddily()
vytvor_ubytovani(seznam_deti)
for oddil in seznam_oddilu.seznam_oddilu:
    oddil.urcit_pocet_chatek()
#print(seznam_oddilu)
'''
