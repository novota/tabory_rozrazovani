import math

from openpyxl import *
import datetime
import numpy as np


# skontroluje jestli dite nema ekvivalentni protejsek v seznamu lidi navic, pokud ano, odstrani ekvivalentni dite
# ze seznamu


def mam_pridat_dite(oddil, dite, margin, maximum_kluku=-1):
	# zkontrolovat, jestli je v oddile jeste dost mista pro dite a jeho kamarady
	# potrebuju return information wether its false cause of space or because there is a kid in the seznam_lidi_navic
	# list
	pocet = 1
	pocet_kluku = 1
	kamarad1 = dite.kamarad1
	kamarad2 = dite.kamarad2
	# calculate how many kids in total we have to accomodate
	if not kamarad1 == -1:
		pocet += 1
		if kamarad1.gender == "H":
			pocet_kluku += 1
	if not kamarad2 == -1:
		pocet += 1
		if kamarad2.gender == "H":
			pocet_kluku += 1
	# otestovat, jestli nebyl prekrocen limit u kluku
	if maximum_kluku != -1 and oddil.pocet_kluku + pocet_kluku > maximum_kluku:
		return False, 1
	if oddil.kapacita < pocet:
		return False, 1  # 1 kdyz je mala kapacita a 0 kdyz je dite v seznamu_lidi_navic
	for index, tabornik in enumerate(oddil.seznam_lidi_navic):
		if (abs(dite.age - tabornik.age) <= margin or dite.age < tabornik.age) and dite.gender == tabornik.gender:
			oddil.seznam_lidi_navic.pop(index)  # odstran ekvivalent a vrat false
			return False, 0
		# to same zkontrolovat u kamaradu ditete, pokud maji ekvivalent v seznamu lidi navic tak nepridavat
		# tweak je otazka jestli kdyz to udelam i u kamaradu, tak treba nenajdu zadnej oddil kam toho cloveka s kamaradem muzu zaradit
		#  pak by se musel snizit margin
		if not kamarad1 == -1 and (abs(kamarad1.age - tabornik.age) <= margin or kamarad1.age < tabornik.age) and kamarad1.gender == tabornik.gender:
			oddil.seznam_lidi_navic.pop(index)  # odstran ekvivalent a vrat false
			return False, 0
		if not kamarad2 == -1 and (abs(kamarad2.age - tabornik.age) <= margin or kamarad2.age < tabornik.age) and kamarad2.gender == tabornik.gender:
			oddil.seznam_lidi_navic.pop(index)  # odstran ekvivalent a vrat false
			return False, 0
	return True, 2


def mam_pridat_dite_trojce_dvojce(oddil, dite, margin, maximum_kluku=-1):
	# zkontrolovat, jestli je v oddile jeste dost mista pro dite a jeho kamarady
	# potrebuju return information wether its false cause of space or because there is a kid in the seznam_lidi_navic list
	pocet = 1
	pocet_kluku = 0
	kamarad1 = dite.kamarad1
	kamarad2 = dite.kamarad2
	if not kamarad1 == -1:
		pocet += 1
	if not kamarad2 == -1:
		pocet += 1
	# otestovat, jestli nebyl prekrocen limit u kluku
	if maximum_kluku != -1 and oddil.pocet_kluku + pocet > maximum_kluku:
		return False, 1
	if oddil.kapacita < pocet:
		return False, 1  # 1 kdyz je mala kapacita a 0 kdyz je dite v seznamu_lidi_navic
	for index, tabornik in enumerate(oddil.seznam_lidi_navic):
		if (abs(dite.age - tabornik.age) <= margin or dite.age < tabornik.age) and dite.gender == tabornik.gender:
			# neodstran ekvivalent a vrat false
			return False, 0
		# to same skontrolovat u kamaradu ditete, pokud maji ekvivalent v seznamu lidi navic tak nepridavat
		# tweak je otazka jestli kdyz to udelam i u kamaradu, tak treba nenajdu zadnej oddil kam toho cloveka s kamaradem muzu zaradit
		#  pak by se musel snizit margin
		if not kamarad1 == -1 and (abs(
				kamarad1.age - tabornik.age) <= margin or kamarad1.age < tabornik.age) and kamarad1.gender == tabornik.gender:
			# neodstran ekvivalent a vrat false
			return False, 0
		if not kamarad2 == -1 and (abs(
				kamarad2.age - tabornik.age) <= margin or kamarad2.age < tabornik.age) and kamarad2.gender == tabornik.gender:
			# neodstran ekvivalent a vrat false
			return False, 0
	return True, 2


# FIXME: je otazka, jestli radsi nehledat deti, ktere jsou vekove co nejpodobnejsi, nez jit proste od konce, asi by to
#  slo spravit tim, ze dite, ktere tady vybereme, nezaradime na konec seznamu, ale dame ho tam, kam vekem patri.
#  Nebo to udelat jako u kluku, ze kdyz zbyva 2 3 mista, tak hledam dvojce trojce
def najit_oddil_pro_switch(seznam_oddilu, dite):
	if dite.name == "Fajtová Natálie":
		print("zde")
	for index, oddil in enumerate(seznam_oddilu):
		vhodny_oddil = True  # indikator, jestli se oddil bude hodit na switch
		# zjistit kolik potrebuju volnych mist
		uvolnit_mist = dite.pocet_kamaradu() + 1 - oddil.kapacita
		uvolneno = 0
		index_konce = len(oddil.seznam_deti) - 1
		while uvolnit_mist > 0:
			temp_dite = oddil.seznam_deti[index_konce]
			# pokud ma dite kamarada, nebrat a jit na dalsi oddil
			if not temp_dite.pocet_kamaradu() == 0:
				vhodny_oddil = False
				break
			else:
				uvolneno += 1
				index_konce -= 1
				uvolnit_mist -= 1
		if vhodny_oddil:
			return oddil, uvolneno

	print("najit_oddil_pro_switch nenaslo vhodny oddil. toto je vzacna a neosetrena situace, jmeno ditete je " + str(dite))
	return None, 0


def vytvorit_seznam_deti_spolu(seznam_deti):
	result = []
	temp_seznam = seznam_deti.copy()
	while len(temp_seznam) > 0:
		dite = temp_seznam.pop(0)
		result.append(dite.pocet_kamaradu_stejneho_pohlavi())
		# odstranit jeste kamarady
		if dite.kamarad1 != -1 and dite.kamarad1.gender == dite.gender:
			temp_seznam.pop(temp_seznam.index(dite.kamarad1))
		if dite.kamarad2 != -1 and dite.kamarad2.gender == dite.gender:
			temp_seznam.pop(temp_seznam.index(dite.kamarad2))
	result.sort(reverse=True)
	return result


def vytvor_pocty_kluku_na_oddil(pocet_kluku, pocet_oddilu):
	seznam = []
	min_pocet = pocet_kluku // pocet_oddilu
	pocet_oddilu_s_klukem_navic = pocet_kluku % pocet_oddilu
	while pocet_oddilu_s_klukem_navic > 0:
		seznam.append(min_pocet + 1)
		pocet_oddilu_s_klukem_navic -= 1
		pocet_oddilu -= 1

	while pocet_oddilu > 0:
		seznam.append(min_pocet)
		pocet_oddilu -= 1

	return seznam


def add_kid_to_oddil(oddil, dite, margin, maximum_kluku=-1):
	# zkontrolovat jestli neni ekvivalentni dite v seznamu lidi navic, pokud ano, nepridavat,
	mam_pridat, duvod = mam_pridat_dite(oddil, dite, margin, maximum_kluku)
	if mam_pridat:
		oddil += dite
		dite.oddil = oddil

		if not dite.kamarad1 == -1:
			oddil += dite.kamarad1
			dite.kamarad1.oddil = oddil
			oddil.seznam_lidi_navic.append(dite.kamarad1)

		if not dite.kamarad2 == -1:
			oddil += dite.kamarad2
			dite.kamarad2.oddil = oddil
			oddil.seznam_lidi_navic.append(dite.kamarad2)
		return True, duvod
	else:
		return False, duvod


def find_triplet(seznam_deti, oddil, margin):
	vysvetleni = -3
	for index, dite in enumerate(seznam_deti):
		if dite.oddil == - 1 and dite.pocet_kamaradu() == 2:
			pridat, vysvetleni = mam_pridat_dite(oddil, dite, margin)  # test jestli neni v seznamu deti navic nejaka shoda
			if pridat:
				return seznam_deti.pop(index)
			else:
				return vysvetleni  # may be only 0
	return vysvetleni


def find_twins(seznam_deti, oddil, margin):
	vysvetleni = -2
	for index, dite in enumerate(seznam_deti):
		if dite.oddil == - 1 and dite.pocet_kamaradu() == 1:
			pridat, vysvetleni = mam_pridat_dite(oddil, dite,
				margin)  # test jestli neni v seznamu deti navic nejaka shoda
			if pridat:
				return seznam_deti.pop(index)
			else:
				return vysvetleni  # may be only 0
	return vysvetleni


# if kid has friend of opposite gender, we dont care
def find_single(seznam_deti, oddil, margin):
	vysvetleni = -1  # -1 = kid without friends doesnt exists, 0 = seznam_navic, 1 = odd_capacity, 2 = succes
	for index, dite in enumerate(seznam_deti):
		if dite.oddil == -1 and dite.pocet_kamaradu_stejneho_pohlavi() == 1:  # 1 is the kid itself
			pridat, vysvetleni = mam_pridat_dite(oddil, dite,
				margin)  # test jestli neni v seznamu deti navic nejaka shoda
			if pridat:
				return seznam_deti.pop(index)
	return vysvetleni


def neexistuje_dite_bez_oddilu(seznam_deti):
	for dite in seznam_deti:
		if dite.oddil == -1:
			return False
	return True


def najit_rozdeleni_b(pocet_deti, seznam_kapacit, result=[]):
	if pocet_deti == 0:
		return result

	for index, element in enumerate(seznam_kapacit):
		# to je kdyz jsme na konci seznamu
		if element[0] == 0:
			break
		# pokud je chatka k dispozici a neni vyhrazena pro holky, pouzit
		if element[1] > 0 and element[2] == 0 and element[0] <= pocet_deti:
			result.append(element[0])
			seznam_kapacit[index][1] -= 1
			vysledek = najit_rozdeleni_b(pocet_deti - element[0], seznam_kapacit, result)
			if vysledek is not None:
				return vysledek
			else:
				result.remove(element[0])
				seznam_kapacit[index][1] += 1
	return None


def najit_rozdeleni_g(seznam_kapacit):
	result = []
	for element in seznam_kapacit:
		while element[1] > 0:
			result.append((element[0], element[2]))
			element[1] -= 1
	return result


def vybalancovat_kapacity(list_kapacit_b, seznam_kapacit):
	pocet_osmicek = list_kapacit_b.count(8)
	# spocitat pocet ctyrek naivne
	pocet_ctyrek = 0
	for index, element in enumerate(seznam_kapacit):
		if element[0] == 4 and element[2] == 0:
			pocet_ctyrek += element[1]
	pocet_ctyrek = pocet_ctyrek // 2
	if pocet_ctyrek % 2 != 0:
		pocet_ctyrek -= 1
	while pocet_ctyrek >= 2:
		list_kapacit_b.remove(8)
		list_kapacit_b.append(4)
		list_kapacit_b.append(4)
		pocet_ctyrek -= 2
		for element in seznam_kapacit:
			if element[0] == 4 and element[2] == 0:
				element[1] -= 2
		for element in seznam_kapacit:
			if element[0] == 8 and element[2] == 0:
				element[1] += 1


# return list_kapacit_b, seznam_kapacit


# creates from input_path string an output_path string, which is the same folder as input and modifies the original
# file name to "FILENAME_oddily.xlsx"
def get_out_path(input_path):
	output_path = ''
	for char in input_path:
		if char == ".":
			break
		output_path += char
	appendix = '_oddily.xlsx'
	output_path += appendix
	return output_path


# data is one column of data which shall be written into existing excel file in the excel path in the target_column
def add_oddily_to_excel(data, excel_path, target_column, vek):
	# get the data length, first element is zero and needs to be ignored
	counter = data.count().values.item(0) - 1

	# open existing excel
	wb = load_workbook(excel_path)
	# load the 1st Sheet
	ws = wb.active

	# write data to ws
	row = 1  # iterator index
	while counter > 0:
		wcell = ws.cell(row + 1, target_column)
		wcell.value = data.at[row, 'seznam']
		counter -= 1
		row += 1
	'''
	# create a new col and insert all ages
	ws.insert_cols(target_column)
	ws.cell(1, target_column).value = "Věk"
	row = 1
	counter = data.count().values.item(0) - 1
	while counter > 0:
		wcell = ws.cell(row+1,target_column)
		wcell.value = vek[row-1]
		counter -= 1
		row += 1
	'''

	# save all changes
	# create a path to save the result
	res_path = get_out_path(excel_path)
	wb.save(res_path)


# input string must be in format "day - nonnumber char - month - dot terminated"
def convert_string_to_month_and_day(input):
	length = len(input)
	index = 0
	assert (length != 0)
	day = ""
	month = ""

	while length > 0 and input[index].isnumeric():
		day += input[index]
		index += 1
		length -= 1
	index += 1
	length -= 1
	while length > 0 and input[index].isnumeric():
		month += input[index]
		index += 1
		length -= 1
	return day, month


def get_datetime(day, month, year):
	datum = day + '-' + month + '-' + year
	res = datetime.datetime.strptime(datum, "%d-%m-%Y")
	return res


# from total nr. of kids and from max. nr. kids in 1 oddil computes the total count of oddily with capacity
# max_kids_oddil and count of oddily with capacity max_kids_oddil - 1
def get_odd_count(kids_total, max_kids_oddil):
	total_oddily_count = math.ceil(kids_total / max_kids_oddil)  # zaokrouhleni vzdy nahoru
	odd_2 = total_oddily_count * max_kids_oddil - kids_total
	odd_1 = total_oddily_count - odd_2
	return odd_1, odd_2


# find kid without oddil with 2,1 or 0 friends. If oddil_kapacity is 3 or 2, prefere a kid with 2 or 1 friend
# returns dite which has no problems being added to oddil,0 means kid was not added due to seznam_lidi navic a nd a kid
# was removed from this seznam, -1 if oddil capacity is 1 and there is no sigle kid
def find_suited_kids(seznam_deti, kapacita_oddilu, margin, oddil):
	if kapacita_oddilu == 3:
		dite = find_triplet(seznam_deti.seznam_deti, oddil, margin)
		if dite == -3:  # there is no triplet in seznam_deti
			return seznam_deti.find_no_oddil_kid(margin, 3, oddil)
		return dite  # dite can be 0 or a kid
	elif kapacita_oddilu == 2:
		dite = find_twins(seznam_deti.seznam_deti, oddil, margin)
		if dite == -2:
			return seznam_deti.find_no_oddil_kid(margin, 2, oddil)
		return dite  # dite can be 0 or a kid or None
	elif kapacita_oddilu == 1:
		return find_single(seznam_deti.seznam_deti, oddil, margin)  # may be vysvetleni (-1,0)
	return seznam_deti.find_no_oddil_kid(margin, 5, oddil)  # may be vysvetleni (0)


# returns kodes and quantities which exceed #3
def verify_friend_codes_quantity(list_of_friend_codes):
	# modify list of codes so that -1 is not in there
	condition = list_of_friend_codes != -1
	list_of_friend_codes = np.extract(condition, list_of_friend_codes)
	# TODO: filter out everything that is not integers
	# convert np array to python list
	list_of_friend_codes = list_of_friend_codes.tolist()
	# get list of friends codes
	friends_code = []
	kolikrat = []
	list_of_friend_codes.sort()
	cur_kod = 0
	for kod in list_of_friend_codes:
		if cur_kod != kod:
			cur_kod = kod
			if list_of_friend_codes.count(cur_kod) > 3:
				friends_code.append(cur_kod)
				kolikrat.append(list_of_friend_codes.count(cur_kod))
	return friends_code, kolikrat


# return a list of friends codes which appear only once in the whole list
def check_single_codes(seznam_deti):
	f_codes = seznam_deti[:, 5]
	result = []
	# modify list of codes so that -1 is not in there
	condition = f_codes != -1
	f_codes = np.extract(condition, f_codes)
	# TODO: filter out everything that is not integers
	# convert np array to python list
	f_codes = f_codes.tolist()
	cur_code = 0
	for code in f_codes:
		if cur_code != code:
			cur_code = code
			if f_codes.count(cur_code) == 1:
				result.append(cur_code)
	return result
