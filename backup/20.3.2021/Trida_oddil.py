from Hlavni_kod.Help_functions import add_kid_to_oddil, find_suited_kids
from Hlavni_kod.Help_functions import najit_oddil_pro_switch
from Hlavni_kod.Help_functions import vytvor_pocty_kluku_na_oddil
from Hlavni_kod.Help_functions import neexistuje_dite_bez_oddilu
from Hlavni_kod.Trida_dite import Dite


class Seznam_oddilu:

	def __init__(self, seznam_oddilu=[]):
		self.seznam_oddilu = seznam_oddilu

	def __iadd__(self, other):
		assert isinstance(other, Oddil)
		self.seznam_oddilu.append(other)
		return self

	def __str__(self):
		result = 'Mame nasledujici oddily: \n'
		for element in self.seznam_oddilu:
			result += str(element)
		return result

	def inicializace_oddilu(self, kapacita, pocet, cislo_oddilu):
		while pocet > 0:
			self.seznam_oddilu.append(Oddil(cislo_oddilu, kapacita))
			pocet -= 1
			cislo_oddilu += 1
		return self

	def clear(self):
		self.seznam_oddilu = []
		return self

	# seradi oddily podle poctu deti v nich od nejmene po nejvice
	def seradit_oddily_vzestupne(self):
		index = 1
		while index < len(self.seznam_oddilu):
			j = index - 1
			while j >= 0:
				if self.seznam_oddilu[j + 1].obsazenost() < self.seznam_oddilu[j].obsazenost():
					temp = self.seznam_oddilu[j]
					self.seznam_oddilu[j] = self.seznam_oddilu[j + 1]
					self.seznam_oddilu[j + 1] = temp
					j -= 1
				else:
					break
			index += 1
		return self

	# razeni holek do oddilu
	def zaradit_holky_do_oddilu_tam_zpet(self, seznam_deti, margin):
		# seznam_deti.seznam_deti.sort(key=lambda x: x.pocet_kamaradu(), reverse=True) # todo ma tohle smysl?
		aktualni_index_oddil = 0
		while not seznam_deti.find_no_oddil_kid(margin, 5) is None:
			oddil = self.seznam_oddilu[aktualni_index_oddil]  # aktualni oddil
			if oddil.kapacita == 0:
				if aktualni_index_oddil + 1 == len(self.seznam_oddilu):
					aktualni_index_oddil = 0
				else:
					aktualni_index_oddil += 1
				continue
			pridat_dite = True

			# find kid with 2,1 or 0 frieds according to the capacity left
			dite = find_suited_kids(seznam_deti, oddil.kapacita, margin, oddil)  # returns kid, -1 or 0

			if pridat_dite:
				if isinstance(dite, Dite):
					povedlose, duvod = add_kid_to_oddil(oddil, dite, margin)  # this should always add kid to oddil
					if not povedlose:
						print("find_suited_kids naslo dite, ktere se melo dat priradit do oddilu, ale nedalo se. Tohle"
						      " se nemelo stat, musim zjistit, kde se stala chyba")
				elif dite == -1:  # oddil capacity == 1 but there is no single kid anymore
					dite = seznam_deti.find_no_house_kid()
					# check reason why we couldnt find any kid
					temp_seznam_oddilu = self.seznam_oddilu.copy()
					temp_seznam_oddilu.sort(key=lambda x: x.avg_age(), reverse=True)  # serad od nejstarsiho
					# najdi oddil pro switch by melo vzdycky najit nejaky oddil, pokud ne, a nemam osetreny
					# vsechny situace, tak bude problem
					vhodny_oddil, uvolneno = najit_oddil_pro_switch(temp_seznam_oddilu, dite)
					if vhodny_oddil is None:
						print("oddil is  none, tohle se nesmi stat")
					# odstran z vhodneho oddilu pocet deti = uvolneno
					while uvolneno > 0:
						temp_dite = vhodny_oddil.seznam_deti[len(vhodny_oddil.seznam_deti) - uvolneno]
						vhodny_oddil -= temp_dite
						temp_dite.oddil = -1
						seznam_deti.seznam_deti.append(temp_dite)  # pridat dite opet do seznamu deti k zarazeni
						uvolneno -= 1
					# po odstraneni deti muzeme pridat nasi dvojci, trojci
					mam_pridat, duvod = add_kid_to_oddil(vhodny_oddil, dite, margin)  # fixme mozna dat margin +inf aby to nekdy nehodilo chybu?
					# if kid is not added due to removing a kid from list_deti_navic
					if not mam_pridat:
						seznam_deti.seznam_deti.insert(0, dite)
			# else dite == 0 and we do nothing. kid from seznam_lidi_navic has been removed and thats it
			if aktualni_index_oddil + 1 == len(self.seznam_oddilu):
				aktualni_index_oddil = 0
			else:
				aktualni_index_oddil += 1

		return self

	def zaradit_kluky_do_oddilu(self, seznam_deti, margin, pocet_kluku_na_oddil, aktualni_index_oddil=0):

		while not len(seznam_deti.seznam_deti) == 0:
			pridat_dite = True
			oddil = self.seznam_oddilu[aktualni_index_oddil]  # aktualni oddil
			# no more kids without oddil in the list
			if seznam_deti.find_no_oddil_kid(margin, 4) is None:
				return self

			# zjistit, jestli nemame prave dve nebo tri posledni mista, abychom pridali dvojci, trojci
			if len(pocet_kluku_na_oddil) == 0:
				assert neexistuje_dite_bez_oddilu(seznam_deti.seznam_deti)

			else:
				maximum_kluku = max(pocet_kluku_na_oddil)
				minimum_kluku = min(pocet_kluku_na_oddil)
			# zkontrolovat, ze oddil neprekrocil kapacitu
			if oddil.pocet_kluku >= maximum_kluku:
				if neexistuje_dite_bez_oddilu(seznam_deti.seznam_deti):
					break
				if aktualni_index_oddil + 1 == len(self.seznam_oddilu):
					aktualni_index_oddil = 0
				else:
					aktualni_index_oddil += 1
				continue

			get_single = False
			get_twins = False
			get_triplet = False

			# TODO: tohle chceme odstranit, kdyz delame jen kluky. jen si nejak dat pozor, aby se rozrazeni povedlo,
			# protoze kapacita je taky omezena. Tohle to resi, alle zase tozpusobuje treba to, ze mam trojci starejch
			# deti cca kolem 15ti a pak jim pridam trojci nejakejch trinactiletejch
			if oddil.pocet_kluku == maximum_kluku - 3:
				get_triplet = True
			elif oddil.pocet_kluku == maximum_kluku - 2:
				get_twins = True
			if oddil.pocet_kluku == minimum_kluku - 3:
				get_triplet = True
			elif oddil.pocet_kluku == minimum_kluku - 2:
				get_twins = True
			if oddil.pocet_kluku == maximum_kluku - 1:
				get_single = True
			elif oddil.pocet_kluku == minimum_kluku - 1:
				get_single = True
			if oddil.pocet_kluku == maximum_kluku:
				pocet_kluku_na_oddil.remove(maximum_kluku)
				continue

			# ted bud hledame dvojci nebo trojci, nebo nam je to jedno
			# capacity > 3
			if not get_triplet and not get_twins and not get_single:
				kid = seznam_deti.find_no_oddil_kid(margin, 4, oddil)  # kid or 0
			# capacity >= 3
			elif get_triplet and not get_twins and not get_single:
				kid = find_suited_kids(seznam_deti, 3, margin, oddil)  # kid is 0 or dite
			# capacity >= 3 or >= 2
			elif get_triplet and get_twins and not get_single:
				kid = find_suited_kids(seznam_deti, 3, margin, oddil)  # kid is 0 or dite
				if isinstance(kid, Dite) and kid.pocet_kamaradu() != 2:
					kid = find_suited_kids(seznam_deti, 2, margin, oddil)
			# capacity == 2 or === 1
			elif not get_triplet and get_twins and get_single:
				kid = find_suited_kids(seznam_deti, 2, margin, oddil)
				if kid is None:
					print("kid is None. capacity is 2 or one and we didnt find a kid with 1 friend, nore one with 0"
					      "friend. This is highly unlikely, but if it happens, we need to deal with it. That means I"
					      "must handle this case by creating new code. This situation is not handeled yet")
			elif not get_triplet and get_twins and not get_single:
				kid = find_suited_kids(seznam_deti, 2, margin, oddil)
				if kid is None:
					print('Neosetrena situace. Hledali jsme twins, nenasli, tak jsme hledali solo dite, taky jsme ho'
					      'nenasli. Pokud se toto stane, tak to budu muset nejak osetrit')
			else:  # capacity == 1
				kid = find_suited_kids(seznam_deti, 1, margin, oddil)
				if kid == - 1:
					print('aktualni oddil je', oddil.cislo_oddilu)
					print(self)
					print(seznam_deti)
					print("neosetrena situace. kapacita oddilu == 1 a nenalezeno dite bez kamaradu. Pokud se to"
					      "stane, budu to muset osetrit")
			# case kid == 0 -> kid from seznam_deti_navic had been deleted
			if kid == 0:
				pridat_dite = False

			if pridat_dite:
				povedlose, duvod = add_kid_to_oddil(oddil, kid, margin, maximum_kluku)
				seznam_deti.delete_kid_with_friends(kid)  # delete from seznam_deti
				if not povedlose:
					print("neosetrena situace. vybrane dite ", kid, " se melo pridat do oddilu, ale nepridalo. Musim "
					                                                "zrevidovat logiku do radku 186")
				if oddil.pocet_kluku == maximum_kluku:
					pocet_kluku_na_oddil.remove(maximum_kluku)
			if aktualni_index_oddil + 1 == len(self.seznam_oddilu):
				aktualni_index_oddil = 0
			else:
				aktualni_index_oddil += 1

		return self

	def naplnit_oddily_na_kapitana_varianta(self, seznam_deti):
		avg_age = seznam_deti.prumerny_vek()

		# oddelit kluky a holky
		seznam_g = seznam_deti.seznam_holek()
		seznam_b = seznam_deti.seznam_kluku()
		# seradit od nejstarsiho po nejmladsi
		seznam_g.seradit_sestupne()
		seznam_b.seradit_sestupne()
		# spocitej marginy
		margin_b = seznam_b.find_margin(len(self.seznam_oddilu))
		margin_g = seznam_g.find_margin(len(self.seznam_oddilu))
		# spocitej kolik kluku muze mit maximalne jeden oddil
		pocet_kluku_na_oddil = vytvor_pocty_kluku_na_oddil(len(seznam_b.seznam_deti), len(self.seznam_oddilu))
		# nejdriv zarad kluky
		self.zaradit_kluky_do_oddilu(seznam_b, margin_b, pocet_kluku_na_oddil)
		# seradit oddily podle nejnizsiho veku po nejvyssi
		# self.seznam_oddilu.sort(key=lambda x: x.avg_age())
		self.seznam_oddilu.sort(key=lambda x: x.cislo_oddilu, reverse=True)
		# clear boys from seznam_deti_navic
		self.clear_seznam_lidi_navic("H")
		# zarad holky
		self.zaradit_holky_do_oddilu_tam_zpet(seznam_g, margin_g)
		return self

	# get the difference between oddil with lowest ang age and highest avg age
	def get_avg_age_diff(self):
		min_avg = 100
		max_avg = 0
		for oddil in self.seznam_oddilu:
			if oddil.avg_age() > max_avg:
				max_avg = oddil.avg_age()
			if oddil.avg_age() < min_avg:
				min_avg = oddil.avg_age()
		return max_avg - min_avg

	def clear_kids(self):
		for oddil in self.seznam_oddilu:
			oddil.clear_all_kids()
		self.seznam_oddilu.sort(key=lambda x: x.cislo_oddilu)
		return self

	def clear_seznam_lidi_navic(self, koho_vyhodit):
		for oddil in self.seznam_oddilu:
			temp_list = []
			for dite in oddil.seznam_lidi_navic:
				if dite.gender == koho_vyhodit:
					temp_list.append(dite)
			for dite in temp_list:
				oddil.seznam_lidi_navic.remove(dite)
		return self


class Oddil:

	def __init__(self, cislo_oddilu=-1, kapacita=15):
		self.kapacita = kapacita
		self.pocet_kluku = 0
		self.seznam_deti = []
		self.pocet_chatek = 0
		self.seznam_cisel_chatek = []
		self.cislo_oddilu = cislo_oddilu
		self.pauzirovani = 0  # kdyz si oddil vybere vic jak jedno dite, pricte se mu sem 1 nebo dva aby priste pauziroval
		self.pauzirovani_b = 0  # separatni counter pro kluky a holky
		self.pauzirovani_g = 0
		self.dalsi_narade = True  # pri rozrazovani urcuje jestli se dalsi priradi kluk nebo holka. Kluk = true, holka = false
		self.seznam_lidi_navic = []
		self.kapacita_kluku = 7

	# holek T54,c48
	# kluku T66, C48

	def __iadd__(self, other):
		assert isinstance(other, Dite)
		if other.gender == "H":
			self.pocet_kluku += 1
		self.seznam_deti.append(other)
		self.kapacita -= 1
		return self

	def __isub__(self, other):
		assert isinstance(other, Dite)
		self.seznam_deti.remove(other)
		self.kapacita += 1
		if other.gender == "H":
			self.pocet_kluku -= 1
		return self

	def __str__(self):
		result = 'Oddil c. {:d} with capacity {:d} including {:d} boys with average age of {:f} je v {:d} chatkach: \n'.format(
			self.cislo_oddilu, self.kapacita, self.pocet_kluku, self.avg_age(), self.pocet_chatek)
		# spocitat, v kolika chatkach mame ubytovany deti

		for element in self.seznam_deti:
			result += str(element)
		result += "\n"
		return result

	def clear_all_kids(self):
		count = 0
		# spocti kluky
		for dite in self.seznam_deti:
			if dite.gender == "H":
				count += 1
		self.pocet_kluku -= count
		self.kapacita += len(self.seznam_deti)
		self.seznam_deti = []
		self.seznam_lidi_navic = []
		return self

	def urcit_pocet_chatek(self):
		for dite in self.seznam_deti:
			if dite.chatka is not None and self.seznam_cisel_chatek.count(dite.chatka.cislo_chatky) == 0:
				self.seznam_cisel_chatek.append(dite.chatka.cislo_chatky)
				self.pocet_chatek += 1
		return self

	def delete(self, dite, valueError="None"):
		assert isinstance(dite, Dite)
		try:
			self.seznam_deti.remove(dite)
		except valueError as v:
			print('tohle dite v seznamu vazne neni!! ', v)
		return self

	def avg_age(self):
		summ = 0
		count = 0
		for element in self.seznam_deti:
			summ += element.age
			count += 1
		if count == 0:
			return -1
		else:
			return summ / count
