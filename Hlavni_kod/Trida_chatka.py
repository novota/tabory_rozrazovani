import Trida_dite
import Help_functions
import csv


# trida, ktera uchovava seznam chatek


class Seznam_chatek:

	def __init__(self, nazev_seznamu="Novy Seznam"):
		self.seznam_chatek = []
		self.nazev_seznamu = nazev_seznamu

	def __iadd__(self, other):
		assert isinstance(other, Chatka)
		self.seznam_chatek.append(other)
		return self

	def __str__(self):
		seznam = 'Mame k dispozici nasledujici chatky s celkovou kapacitou {:s}: \n'.format(str(self.kapacita_tabora()))
		for element in self.seznam_chatek:
			seznam += str(element)
		return seznam

	def cast_chatek(self, kolik_chatek):
		result = Seznam_chatek(self.nazev_seznamu)
		index = 0
		while kolik_chatek > 0:
			result += self.seznam_chatek[index]
			index += 1
			kolik_chatek -= 1
		return result

	def load(self):
		with open('Novy_Radov_Hornak.csv', 'r', newline='') as f:
			reader = csv.reader(f)
			list_chatek = [row for row in reader]
		# projet seznam list_chatek a vytvorit nove chatky
		for index, element in enumerate(list_chatek):
			self.seznam_chatek.append(Chatka(list_chatek[index][1], list_chatek[index][0], list_chatek[index][2], float(list_chatek[index][3])))
		return self

	def save(self):
		# vyextrahuju jen cislo chatky a jeji kapacitu
		seznam = [[0 for x in range(4)] for y in range(len(self.seznam_chatek))]
		for row, chatka in enumerate(self.seznam_chatek):
			seznam[row][0] = chatka.cislo_chatky
			seznam[row][1] = chatka.pocet_mist
			seznam[row][2] = chatka.pohlavi
			seznam[row][3] = chatka.max_age

		# ted to ulozim do csv
		with open('Novy_Radov_Hornak.csv', 'w', newline='') as f:
			writer = csv.writer(f)
			writer.writerows(seznam)
		return self

	# najde jeste nedodelane chatky a jednu dalsi prazdnou
	def najit_idealni_chatku(self, kid, pocet_kamaradu, rozdil, modifier, list_kapacit_b, list_kapacit_g):

		for index, chatka in enumerate(self.seznam_chatek):
			# skontrolovat, ze muzeme pouzit kapacitu chatky kdyz je nova prazdna chatka
			# prazdna chatka s omezenim
			if chatka.max_age != 0 and chatka.pocet_deti == 0:
				if kid.gender == chatka.pohlavi and chatka.max_age + modifier >= kid.vek_nejstersiho_z_kamaradu():
					# nemusime kontrolovat, chatka je pouze pro holky
					list_kapacit_g.remove((int(chatka.pocet_mist), 12))
					return chatka
			# poloplna chatka s omezenim
			if chatka.max_age != 0 and chatka.pocet_deti != 0:
				if kid.gender == chatka.pohlavi and chatka.max_age + modifier >= kid.vek_nejstersiho_z_kamaradu() and \
						int(chatka.pocet_mist) - chatka.pocet_deti >= 1 + pocet_kamaradu:
					return chatka
			# prazdna chatka bez omezeni
			if chatka.max_age == 0 and chatka.pocet_deti == 0:
				if kid.gender == "H" and list_kapacit_b.count(int(chatka.pocet_mist)) != 0:
					list_kapacit_b.remove(int(chatka.pocet_mist))
					return chatka
				if kid.gender == "D" and list_kapacit_g.count((int(chatka.pocet_mist), 0)) != 0:
					list_kapacit_g.remove((int(chatka.pocet_mist), 0))
					return chatka
			# poloplna chatka bez omezeni
			if chatka.max_age == 0 and chatka.pocet_deti != 0:
				if kid.gender == chatka.pohlavi and int(chatka.pocet_mist) - chatka.pocet_deti >= 1 + pocet_kamaradu and \
						(kid.age > chatka.nejstarsi_kid() and kid.age - chatka.nejmladsi_kid() < rozdil or
						 kid.age < chatka.nejstarsi_kid() and chatka.nejstarsi_kid() - kid.age < rozdil):
					return chatka

		return None

	# vzit prvni oddil

	def vytvorit_ubytovani(self, seznam_deti):
		kid = seznam_deti.find_no_house_kid()
		rozdil = 1.5
		modifier = 0  # na chatky s omezenim

		# TODO: nejdrive rozradit dvojce a trojce, rozprostrit je pres vsechny chatky, vzdy treba max 3 oddily na jednu
		# chatku, potom dozaradit single deti tak, ze vezmu vzdy dva oddily najednou, abych videl trochu dopredu

		# nejdrive musime zjistit, do jakych chatek budeme davat kluky a holky, aby nam to vyslo, tzn, aby nezbyla posledni
		# holka a nebylo misto jen u kluku
		seznam_g = seznam_deti.seznam_holek()
		seznam_b = seznam_deti.seznam_kluku()

		# najit kapacity chatek a jejich pocet
		seznam_kapacit, b = self.extrahovat_kapacitu_a_pocet()
		seznam_kapacit.sort(key=lambda x: x[0], reverse=True)
		list_kapacit_b = Help_functions.najit_rozdeleni_b(len(seznam_b.seznam_deti), seznam_kapacit)
		Help_functions.vybalancovat_kapacity(list_kapacit_b, seznam_kapacit)
		list_kapacit_g = Help_functions.najit_rozdeleni_g(seznam_kapacit)

		while kid is not None:

			pocet_kamaradu = 0
			kid_age = kid.age
			kid_gender = kid.gender
			kamarad1 = kid.kamarad1
			kamarad2 = kid.kamarad2

			if kamarad1 == -1:
				kamarad1 = None
			elif kid_gender == kamarad1.gender:
				pocet_kamaradu += 1
			else:
				kamarad1 = None
			if kamarad2 == -1:
				kamarad2 = None
			elif kid_gender == kamarad2.gender:
				pocet_kamaradu += 1
			else:
				kamarad2 = None

			# najit idealni chatku, kde je bud jeste misto + jednu dalsi prazdnou
			chatka = self.najit_idealni_chatku(kid, pocet_kamaradu, rozdil, modifier, list_kapacit_b, list_kapacit_g)
			if chatka is None:
				print("nepovedlo se najit zadnou vhodnou chatku pro " + kid.name + " :(")
				return self

			# pridat deti a kamarady do chatky
			chatka += kid

			# podivat se, kolik je jeste volnych mist a pribrat k diteti jeste dalsi lidi z oddilu
			dalsi_mista_k_dispozici = int(chatka.pocet_mist) - chatka.pocet_deti
			# pokud mam nejaka dalsi volna mista, kouknout, kolik lidi z oddilu jeste mam
			if dalsi_mista_k_dispozici > 0:
				chatka.najit_a_zaradit_max_deti_do_chatky(kid, dalsi_mista_k_dispozici, rozdil, modifier)
			# najit kolik deti v oddile stejneho pohlavi mam k dispozici

			kid = seznam_deti.find_no_house_kid()

		return self

	def inicializace_chatek(self):
		nrChatek = int(input("Kolik mame celkem k dispozici chatek?"))
		while True:
			omezeni = input("ma chatka nejake vekove omezeni nebo omezeni pohlavi? y/n")
			if omezeni == "y":
				max_age = input("jaky je maximalni vek teto chatky? 0 pokud neni")
				gender = input("jake je pohlavi teto chatky? neurceno pokud neni")
			else:
				max_age = 0
				gender = "neurceno"
			pocet_mist = int(input("Kolik lidi se vejde do chatky?"))
			pocet_chatek = int(input("Kolik mame chatek, kam se vejde {:d} lidi?".format(
				pocet_mist)))  # TODO: kontrola , ze existuje dostatecne mnozstvi chatek
			nrChatek -= pocet_chatek
			cislo = input("Maji chatky cislo? :y: pro cislo, :n: pro string")
			if cislo == "n":
				while pocet_chatek > 0:
					nazev_chatky = input("zadej nazev chatky")
					self.seznam_chatek.append(Chatka(pocet_mist, nazev_chatky, gender, max_age))
					pocet_chatek -= 1
			else:
				pocatecni_cislo = int(input("Pocatecni cislo chatky?"))
				while pocet_chatek > 0:
					self.seznam_chatek.append(Chatka(pocet_mist, str(pocatecni_cislo), gender, max_age))
					pocatecni_cislo += 1
					pocet_chatek -= 1

			if nrChatek <= 0:
				break
		return self

	def extrahovat_kapacitu_a_pocet(self):
		result = [[0 for x in range(4)] for y in range(10)]
		omezena_kapacita = 0
		for index, chatka in enumerate(self.seznam_chatek):
			kapacita = int(chatka.pocet_mist)
			max_age = int(chatka.max_age)
			gender = chatka.pohlavi
			# zaznamenat mnozstvi kapacit, kde je vekove omezeni
			if not max_age == 0:
				omezena_kapacita += kapacita
			# zaradit chatku do seznamu result
			for row, element in enumerate(result):
				if element[0] == kapacita and max_age == element[2] and gender == element[3]:  # we found the same input, add one
					element[1] += 1
					break
				elif element[0] == 0:  # we didnt find same chatka, add it in the first free line
					element[0] = kapacita
					element[1] += 1
					element[2] = max_age
					element[3] = gender
					break  # and stop the loop
		return result, omezena_kapacita


class Chatka:

	def __init__(self, pocet_mist=-1, cislo_chatky="-1", pohlavi="neurceno", max_age=0):
		self.vekovy_soucet = 0
		self.pocet_mist = pocet_mist
		self.cislo_chatky = cislo_chatky
		self.pohlavi = pohlavi
		self.seznam_deti = []
		self.pocet_deti = 0
		self.oddily_v_chatce = []
		self.max_age = max_age

	def __iadd__(self, other):
		assert isinstance(other, Trida_dite.Dite)
		if self.pocet_deti == self.pocet_mist:
			print("kapacita chatky naplnena, neni mozne pridat {:s} do chatky {:s}".format(str(other), str(self)))
			return self
		# if other.oddil.seznam_cisel_chatek.count(str(self.cislo_chatky)) == 0:
		#   other.oddil.seznam_cisel_chatek.append(str(self.cislo_chatky))
		# other.oddil.pocet_chatek += 1
		self.seznam_deti.append(other)
		self.pocet_deti += 1
		self.vekovy_soucet += other.age
		other.chatka = self
		# pokud ma dite kamarady stejneho pohlavi, musi spolu do chatky
		if other.kamarad1 != -1 and other.kamarad1.gender == other.gender:
			self.seznam_deti.append(other.kamarad1)
			self.pocet_deti += 1
			self.vekovy_soucet += other.age
			other.kamarad1.chatka = self
		if other.kamarad2 != -1 and other.kamarad2.gender == other.gender:
			self.seznam_deti.append(other.kamarad2)
			self.pocet_deti += 1
			self.vekovy_soucet += other.age
			other.kamarad2.chatka = self
		# add cislo oddilu in the list every rtime
		self.oddily_v_chatce.append(other.oddil.cislo_oddilu)

		if self.pohlavi == "neurceno":
			self.pohlavi = other.gender
		return self

	def __isub__(self, other):
		assert isinstance(other, Trida_dite.Dite)
		self.seznam_deti.remove(other)
		self.oddily_v_chatce.remove(other.oddil.cislo_oddilu)  # remove its oddil number from the list
		self.pocet_deti -= 1
		self.vekovy_soucet -= other.age
		if len(self.seznam_deti) == 0:  # if no kid is remaining in da house reset the gender
			if self.max_age == 0:
				self.pohlavi = "neurceno"
			else:
				self.pohlavi = "female"
		other.chatka = None
		return self

	def __str__(self):
		hdr = 'Chatka c. {:s} s pohlavim {:s} zbyva {:d} volnych mist a vekovym prumerem {:f} obsahuje pionyry: \n'.format(str(self.cislo_chatky), self.pohlavi,
			int(self.pocet_mist) - int(self.pocet_deti), self.age_avg())
		for element in self.seznam_deti:
			hdr += str(element) + '\n'
		return hdr

	def is_empty(self):
		return self.pocet_deti == 0

	# returns list of oddils in the house
	def chatka_oddily(self):
		oddily = []
		for cislo_oddilu in self.oddily_v_chatce:
			if oddily.count(cislo_oddilu) == 0:
				oddily.append(cislo_oddilu)
		return oddily

	def age_avg(self):
		if self.pocet_deti != 0:
			return self.vekovy_soucet / self.pocet_deti
		else:
			return 0

	def nejmladsi_kid(self):
		assert not len(self.seznam_deti) == 0, "v chatce nikdo neni, neptej se na nejmladsiho"
		min_age = 1000
		for dite in self.seznam_deti:
			if dite.age < min_age:
				min_age = dite.age
		return min_age

	def nejstarsi_kid(self):
		assert not len(self.seznam_deti) == 0, "v chatce nikdo neni, neptej se na nejstarsiho"
		max_age = -1000
		for dite in self.seznam_deti:
			if dite.age > max_age:
				max_age = dite.age
		return max_age

	def najit_a_zaradit_max_deti_do_chatky(self, kid, mista_k_dispozici, max_vek_rozdil, modifier):
		kopie_seznamu_deti = kid.oddil.seznam_deti.copy()
		deti_k_dispozici = []
		nejmladsi_kid_chatka_vek = self.nejmladsi_kid()
		chatka = self

		# zjistit, kolik deti mam maximalne k dispozici, chatka je bud s omezenim nebo bez
		for index, dite in enumerate(kopie_seznamu_deti):
			# chatka s omezenim
			if self.max_age != 0:
				if dite.chatka is None and dite.vek_nejstersiho_z_kamaradu() < self.max_age + modifier and \
						dite.pocet_kamaradu_stejneho_pohlavi() <= mista_k_dispozici:
					# pridat dite a jeho kamarady do seznamu a smazat z kopie seznamu deti
					deti_k_dispozici.append(kopie_seznamu_deti.pop(index))
					if dite.kamarad1 != -1 and dite.kamarad1.gender == dite.gender:
						deti_k_dispozici.append(kopie_seznamu_deti.pop(kopie_seznamu_deti.index(dite.kamarad1)))
					if dite.kamarad2 != -1 and dite.kamarad2.gender == dite.gender:
						deti_k_dispozici.append(kopie_seznamu_deti.pop(kopie_seznamu_deti.index(dite.kamarad2)))

			elif dite.chatka is None and abs(dite.vek_nejstersiho_z_kamaradu() - nejmladsi_kid_chatka_vek) < max_vek_rozdil and \
					dite.gender == self.pohlavi and dite.pocet_kamaradu_stejneho_pohlavi() <= mista_k_dispozici:
				deti_k_dispozici.append(kopie_seznamu_deti.pop(index))
				if dite.kamarad1 != -1 and dite.kamarad1.gender == dite.gender:
					deti_k_dispozici.append(kopie_seznamu_deti.pop(kopie_seznamu_deti.index(dite.kamarad1)))
				if dite.kamarad2 != -1 and dite.kamarad2.gender == dite.gender:
					deti_k_dispozici.append(kopie_seznamu_deti.pop(kopie_seznamu_deti.index(dite.kamarad2)))

		deti_k_dispozici.sort(key=lambda x: x.pocet_kamaradu_stejneho_pohlavi(), reverse=True)
		if len(deti_k_dispozici) > 0:
			# bud mame vic deti k dispozici nez je mista
			if len(deti_k_dispozici) > mista_k_dispozici:
				# bud se povede chatku presne zaplnit, nebo kvuli kamaradum nechame 2(nebo 3?) mista volna, v nejhorsim
				# nechame jedno misto volne
				# vytvrime si seznam kde jsou jen pocty deti, co musi byt spolu
				pocty_deti_spolu = Help_functions.vytvorit_seznam_deti_spolu(deti_k_dispozici)
				# 1. zkusme chatku presne zaplnit
				temp_mista = mista_k_dispozici
				dite_ubytovano = False
				# tweak mozna smazat podminku aby nezustavalo jedno volne misto
				for index, dite in enumerate(deti_k_dispozici):
					if abs(dite.pocet_kamaradu_stejneho_pohlavi() - temp_mista) != 1 and dite.pocet_kamaradu_stejneho_pohlavi() <= temp_mista \
							and dite.chatka is None:
						chatka += dite
						dite_ubytovano = True
						temp_mista -= dite.pocet_kamaradu_stejneho_pohlavi()
				# pokud nikdo neubytovan, udelat znova bez podminky na nenechani jednoho mista
				if not dite_ubytovano:
					for index, dite in enumerate(deti_k_dispozici):
						if dite.pocet_kamaradu_stejneho_pohlavi() <= temp_mista \
								and dite.chatka is None:
							chatka += dite
							temp_mista -= dite.pocet_kamaradu_stejneho_pohlavi()
		return self

	# return true if the house is already full
	def chatka_is_full(self):
		return self.pocet_deti >= int(self.pocet_mist)
