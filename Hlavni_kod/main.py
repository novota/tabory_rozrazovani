# -*- coding: utf-8 -*-
"""
Created on Tue Oct 29 12:09:24 2019

@author: petrnovota
"""
import pandas as pd
from openpyxl import *

import Help_functions
import report_creator
import Trida_chatka
import Trida_dite
import Trida_oddil

import datetime
import numpy as np
import tkinter as tk
from tkinter.filedialog import askopenfilename

# TODO: pridat kontrolu, ze maji maximalne 3 deti stejny friends code

# globalni promene
# offset = pocet dni od tabora od ktereho mam data na testovani, tak aby sedel vek taborniku k datu konani tabora
global file_name, offset, excel_wb, excel_ws, seznamdeti, curr_excel_sheet_name
seznam_oddilu = Trida_oddil.Seznam_oddilu()
global seznam_deti_in_numpy
global tabor_date
global max_kids_oddil, odd_1, odd_2, pocatecni_cislo_odd


max_lidi_v_oddile = -1


# tkinter stuff
def end():
	main.destroy()


def OpenFile():
	list_of_codes.delete(0,'end')
	list_of_sheet_names.delete(0, 'end')
	global file_name, seznam_deti_in_numpy, excel_wb
	file_name = askopenfilename(
		filetypes=(("excel files", "*.xlsx"), ("All Files", "*.*")),
		title="nacti taborovy soubor"
	)
	# Using try in case user types in unknown file or closes without choosing a file.

	try:
		excel_wb = load_workbook(filename=file_name)
		sheet_names = excel_wb.sheetnames
		for sheet_name in sheet_names:
			list_of_sheet_names.insert('end', sheet_name)
		info_panel.set('Soubor byl uspesne nahran')
		l_info_panel.config(font=("Courier", 12), fg='green')

		"""
		seznam_xlsx = pd.read_excel(file_name)
		seznam_komplet = seznam_xlsx.loc[:, ('Jméno', 'R', 'M', 'D', 'S', 'Kód', 'Město')]
		seznam_komplet.fillna(-1, inplace=True)  # nahradi nan hodnoty -1kou
		seznam_deti_in_numpy = seznam_komplet.values

		# spocitej kolik je deti v seznamu a zobraz tuto informaci
		(rows, cols) = seznam_deti_in_numpy.shape
		l01['text'] = rows
		"""
	except:
		info_panel.set('Nastala chyba pri načítání souboru. Zkontroluj názvy sloupců. Musí být: '
		                       'Jméno, R, M, D, S, Kód')
		l_info_panel.config(font=("Courier", 14), fg='red')
		print("No file exists")


def potvrd_zadani():
	global tabor_date, offset, odd_1, odd_2, max_kids_oddil, pocatecni_cislo_odd, seznamdeti
	try:
		index = list_of_dates.curselection()
		text = list_of_dates.get(index)
		list_of_dates.see(index)
	except:
		info_panel.set('Musíš vybrat nějaký termín konání tábora!')
		l_info_panel.config(font=("Courier", 14), fg='red')
		return
	# get day and month from the string
	day, month = Help_functions.convert_string_to_month_and_day(text)  # day and month are both strings
	try:
		year_str = year.get()
		test = year_str[3]
		# create string of date convertible to datetime object. format
		tabor_date = Help_functions.get_datetime(day, month, year_str)
		text = "Datum tábora: " + tabor_date.date().strftime('%d/%m/%Y')
		info_panel.set(text)
		l_info_panel.config(font=("Courier", 12), fg='green')
		# compute offset
		# TODO mozna bysme meli dat vypocet offsetu mimo tento try except blok
		offset = datetime.date.today() - tabor_date.date()
		offset = offset.days.real
	except:
		info_panel.set('Vybral jsi den ' + day + ' a mesíc ' + month + ' ale musíš ještě zadat rok konání tábora ve formátu RRRR!')
		l_info_panel.config(font=("Courier", 14), fg='red')

	# inicializace oddilu a seznamu deti a overeni poctu kodu

	try:
		seznamdeti = Trida_dite.Seznam_deti().inicializace_seznamu_deti(seznam_deti_in_numpy, offset)

	except:
		info_panel.set('Nejdříve načti soubor s daty, vyber termín tábora a potvrď tlačítkem OK')
		l_info_panel.config(font=("Courier", 14), fg='red')
		return

	# get the number of oddilu and create a list of oddilu
	try:
		pocatecni_cislo_odd = int(starting_nr_odd.get())
		# odd_1 is the number of oddily with capacity max_kids_oddil. odd_2 is the number of oddily with capacity max_kids_oddil - 1
		(rows, cols) = seznam_deti_in_numpy.shape  # how many kids are there in total
		odd_1, odd_2, max_kids_oddil = Help_functions.get_odd_count(rows, int(max_oddilu.get()))

		l6['text'] = str(odd_2) + ' po ' + str(max_kids_oddil) + ' dětech a ' + str(odd_1) + ' po ' + str(max_kids_oddil - 1) + ' dětech'

		if odd_1 < 0 or odd_2 < 0:
			info_panel.set('Vyšlo nám záporné množství dětí v oddílu. Zvyš nebo sniř maximální počet dětí na oddíl a potvrď znovu')
			l_info_panel.config(font=("Courier", 13), fg='red')
			return

		info_panel.set('Zadani probehlo uspesne. Muzes vytvorit oddily')
		l_info_panel.config(font=("Courier", 13), fg='green')
		print(seznam_oddilu)
	except:
		info_panel.set('Načti soubor s daty a vyplň max. počet dětí na oddíl a počáteční číslo oddílu')


def on_selection(event):
	global excel_wb, excel_ws, seznam_deti_in_numpy, curr_excel_sheet_name

	list_of_codes.delete(0,'end')
	curr_excel_sheet_name = list_of_sheet_names.get(event.widget.curselection())
	excel_ws = excel_wb[curr_excel_sheet_name]
	info_panel.set("vybral jsi list " + str(excel_ws))

	columns_needed = ['Jméno', 'R', 'M', 'D', 'S', 'Kód']
	vysledny_seznam = []

	for element in columns_needed:
		current_column = Help_functions.get_data_from_column(excel_ws, element)
		if current_column is None:
			info_panel.set('Nastala chyba pri načítání souboru. Zkontroluj názvy sloupců. Musí být: '
			                       'Jméno, R, M, D, S, Kód')
			l_info_panel.config(font=("Courier", 14), fg='red')
			return
		vysledny_seznam.append(current_column)

	seznam_deti_in_numpy = np.array(vysledny_seznam).T

	# spocitej kolik je deti v seznamu a zobraz tuto informaci
	(rows, cols) = seznam_deti_in_numpy.shape
	l01['text'] = rows

	# clear list of codes
	if list_of_codes.size() != 0:
		list_of_codes.delete(0, list_of_codes.size())
	l7['text'] = 'seznam problematických kódů'

	# clear pocet oddilu
	l6['text'] = ""

	# make sure there is maximum 3 same friends codes, if not print them and end function with return
	# check if there are code which appear only once and print them
	singl_kody = Help_functions.check_single_codes(seznam_deti_in_numpy)
	if len(singl_kody) > 0:
		for index, code in enumerate(singl_kody):
			text = 'kód ' + str(int(code)) + ' je tu jen jednou'
			list_of_codes.insert('end', text)
		l7['text'] = 'Seznam problematických kódů. Prověř!!'
	print(seznam_deti_in_numpy[:, 5])
	friends_code, kolikrat = Help_functions.verify_friend_codes_quantity(seznam_deti_in_numpy[:, 5])
	if len(friends_code) > 0:
		for index, code in enumerate(friends_code):
			text = 'kód ' + str(int(code)) + ' je tu ' + str(kolikrat[index]) + 'x'
			list_of_codes.insert('end', text)
		info_panel.set('některé kódy jsou v seznamu více než 3x viz. seznam kódů navíc. Oprav a zkus to znovu')
		l_info_panel.config(font=("Courier", 14), fg='red')
		return

def vytvor_oddily():
	global seznam_deti_in_numpy, offset, max_kids_oddil, odd_1, odd_2, pocatecni_cislo_odd
	# clear list of codes
	if list_of_codes.size() != 0:
		list_of_codes.delete(0, 'end')
	l7['text'] = 'seznam problematických kódů'

	try:
		seznam_oddilu.inicializace_oddilu(max_kids_oddil, odd_2, pocatecni_cislo_odd)
		seznam_oddilu.inicializace_oddilu(max_kids_oddil - 1, odd_1, pocatecni_cislo_odd + odd_2)
		test_value = seznam_oddilu.seznam_oddilu[0]  # iff array has not been initialized yet, this yields error

	except:
		info_panel.set('Nejdříve načti soubor s daty, vyber termín tábora a potvrď tlačítkem OK')
		l_info_panel.config(font=("Courier", 13), fg='red')
		return

	# figure out how many kids born each year are there and calculate average count for each year
	average_counts_of_kids_for_each_year = seznamdeti.get_average_counts_of_kids_for_each_year(len(seznam_oddilu.seznam_oddilu))
	min_error = (100000, -1, -1, "normalni")  # (min_error, margin_g, margin_b)

	info_panel.set("probíhá hledání ideálního rozložení dětí do oddílů")
	# prirazeni deti do oddilu
	for margin_g in np.arange(0.4, 1.4, 0.05):  #0.4 az 1.6
		for margin_b in np.arange(0.4, 1.4, 0.05):
			varianta = "normalni"
			seznam_oddilu.naplnit_oddily_na_kapitana_varianta(seznamdeti, margin_g, margin_b)

			if seznam_oddilu.rozrazeni_not_possible:
				varianta = "nejdrive_kody"
				seznam_oddilu.rozrazeni_not_possible = False
				seznam_oddilu.clear_kids()
				seznam_oddilu.naplnit_oddily_na_kapitana_nejdriv_kody(seznamdeti, margin_g, margin_b)

			if not Help_functions.all_kids_have_oddil(seznamdeti):
				print("not a valid rozrazeni")
				seznam_oddilu.clear_kids()
				continue

			error = seznam_oddilu.calculate_error_kids_for_each_year(average_counts_of_kids_for_each_year)
			if error < min_error[0]:
				min_error = (error, margin_g, margin_b, varianta)

			seznam_oddilu.clear_kids()

	if min_error[1] == -1:
		print(f"rozrazeni se nepvedlo pro zadny margin")
		info_panel.set('Roozrazeni se nepovedlo pro zadny margin. moc zakodovanych deti')
		l_info_panel.config(font=("Courier", 13), fg='red')
		return

	print(f"best margin_g is {min_error[1]}, margin_b is {min_error[2]} min error is {min_error[0]}")
	if min_error[3] == "normalni":
		seznam_oddilu.naplnit_oddily_na_kapitana_varianta(seznamdeti, min_error[1], min_error[2])
	else:
		seznam_oddilu.naplnit_oddily_na_kapitana_nejdriv_kody(seznamdeti, min_error[1], min_error[2])

	for oddil in seznam_oddilu.seznam_oddilu:
		oddil.seznam_deti.sort(key=lambda x: x.age, reverse=True)
	seznam_oddilu.seznam_oddilu.sort(key=lambda x: x.cislo_oddilu)

	# vytvor excel
	# prevod seznamu do pandas data frame
	a1 = pd.DataFrame(seznamdeti.vystup_2D_pro_excel())
	odd = a1.loc[:, 0]
	odd = pd.DataFrame({'seznam': odd.values})

	# for testing purpose create a array with ages of kids and add it to a column in output
	vek = []
	for di in seznamdeti.seznam_deti:
		vek.append(di.age)

	result = Help_functions.add_oddily_to_excel(odd, file_name, excel_wb, excel_ws, vek)
	report_creator.create_report(seznam_oddilu, file_name, curr_excel_sheet_name)


	if not result:
		info_panel.set('Oddily nemohly být zapsány to excelu, neexistuje sloupec s označením "O", nebo "Odd')
		l_info_panel.config(font=("Courier", 14), fg='red')
	else:
		info_panel.set('Oddily byly úspěšně vytvořeny!')
		l_info_panel.config(font=("Courier", 14), fg='green')
		# check if there are code which appear only once and print them
		singl_kody = Help_functions.check_single_codes(seznam_deti_in_numpy)
		if len(singl_kody) > 0:
			for index, code in enumerate(singl_kody):
				text = 'kód ' + str(int(code)) + ' je tu jen jednou'
				list_of_codes.insert('end', text)
			l7['text'] = 'Seznam samostatných kódů. Prověř!!'
			info_panel.set('Oddily byly úspěšně vytvořeny, ale ověř kódy, které jsou jen jednou!')
			l_info_panel.config(font=("Courier", 14), fg='green')
	print(seznam_oddilu)
	seznam_oddilu.clear()

	return


def vytvor_ubytovani(seznam_deti):
	# serad deti podle veku od nejmensich po nejvetsi a podle oddilu
	seznam_deti.seznam_deti.sort(key=lambda x: (x.oddil.cislo_oddilu, x.age))
	# print(seznam_deti)

	# inicializace a ulozeni radova
	# seznam_Radov = Seznam_chatek("Novy Radov Hornak")
	# seznam_Radov.inicializace_chatek()
	# seznam_Radov.save()
	novy_seznam = Trida_chatka.Seznam_chatek("Novy_Radov_Hornak.csv").load()
	zkraceny_seznam = novy_seznam.cast_chatek(20)  # tweak tady muzu vybrat podskupinu chatek
	zkraceny_seznam.vytvorit_ubytovani(seznam_deti)


# print(zkraceny_seznam)
# seznam_podseznamu, seznam_rozlozeni = novy_seznam.naplnit_chatky(seznam_deti, 4)
# print(seznam_deti)


# tkinter


main = tk.Tk()
main.title("Tábory rozřazování")
label = tk.Label(main, height='10', width='40', bg='white')

max_oddilu = tk.StringVar()
starting_nr_odd = tk.StringVar()
total_kids = tk.StringVar()
year = tk.StringVar()
info_panel = tk.StringVar()


b1 = tk.Button(main, text="end", command=end, height='5', width='20')
b1.grid(row=0, rowspan=2, column=0, sticky='we')
b2 = tk.Button(main, text="otevri soubor", command=OpenFile, height='5', width='20')
b2.grid(row=2, rowspan=2, column=0, sticky='we')
b4 = tk.Button(main, text="vytvor oddily", command=vytvor_oddily, height='5', width='20')
b4.grid(row=4, rowspan=2, column=0, sticky='we')
b5 = tk.Button(main, text="OK", command=potvrd_zadani)
b5.grid(row=6, column=2, sticky='we')

l0 = tk.Label(main, text='v seznamu je celkem pionýrů')
l0.grid(row=0, column=1, sticky='we')
l01 = tk.Label(main, text='0')
l01.grid(row=0, column=2, sticky='we')
l1 = tk.Label(main, text='oddíly')
l1.grid(row=1, column=1, sticky='we')
l2 = tk.Label(main, text='celkový počet oddílů')
l2.grid(row=2, column=1, sticky='we')
l3 = tk.Label(main, text='počáteční číslo oddílu')
l3.grid(row=3, column=1, sticky='we')
l_info_panel = tk.Label(main, textvariable=info_panel)
l_info_panel.grid(row=7, column=0, columnspan=4)
l4 = tk.Label(main, text='Rok konání tábora')
l4.grid(row=5, column=1, sticky='we')
l5 = tk.Label(main, text='Vyber termín konání tábora')
l5.grid(row=4, column=1, sticky='we')
l6 = tk.Label(main, text='')
l6.grid(row=1, column=2, sticky='we')
l7 = tk.Label(main, text='seznam kódů navíc')
l7.grid(row=3, column=3, sticky='we')

entry2 = tk.Entry(main, textvariable=max_oddilu)
entry2.grid(row=2, column=2)
entry3 = tk.Entry(main, textvariable=starting_nr_odd)
entry3.grid(row=3, column=2)
entry4 = tk.Entry(main, textvariable=year)
entry4.grid(row=5, column=2)

# scrollbar widget
choose_date_scroll = tk.Scrollbar(main, orient="horizontal")
list_of_dates = tk.Listbox(main, height=3, yscrollcommand=choose_date_scroll.set)
choose_date_scroll['command'] = list_of_dates.yview
# create elements of the scrollbar widget
list_of_dates.insert('end', '4.7.')
list_of_dates.insert('end', '18.7.')
list_of_dates.insert('end', '25.7.')
list_of_dates.insert('end', '1.8.')
list_of_dates.insert('end', '8.8.')
list_of_dates.insert('end', '15.8.')
list_of_dates.insert('end', '22.8.')
# show result
list_of_dates.grid(row=4, column=2)
choose_date_scroll.grid(row=4, column=2)

# another scrollbar widget
kody_navic = tk.Scrollbar(main, orient="horizontal")
list_of_codes = tk.Listbox(main, height=3, yscrollcommand=kody_navic.set, exportselection=False)
kody_navic['command'] = list_of_codes.yview()
# show result
list_of_codes.grid(row=4, column=3)
kody_navic.grid(row=4, column=3)

# scrollbar widget for sheet names
sheet_names_box = tk.Scrollbar(main, orient="horizontal")
list_of_sheet_names = tk.Listbox(main, height=5, yscrollcommand=sheet_names_box.set, exportselection=False)
list_of_sheet_names.bind('<<ListboxSelect>>', on_selection)
# show result
list_of_sheet_names.grid(row=0, column=3)
sheet_names_box.grid(row=0, column=3)


main.mainloop()

'''
seznam_deti, seznam_oddilu = vytvor_oddily()
vytvor_ubytovani(seznam_deti)
for oddil in seznam_oddilu.seznam_oddilu:
    oddil.urcit_pocet_chatek()
#print(seznam_oddilu)
'''
