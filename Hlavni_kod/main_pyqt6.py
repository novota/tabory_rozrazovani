
import sys

from controller import Controller
from model import Model
from view import View

from PyQt6.QtWidgets import QApplication


def set_dark_mode(app):
    # Set the dark theme
    app.setStyleSheet("""
        QWidget {
            background-color: #2b2b2b;
            color: #b1b1b1;
            border: 1px solid #3a3a3a;
        }
        QPushButton {
            background-color: #454545;
            border-style: outset;
            border-width: 2px;
            border-radius: 10px;
            border-color: #454545;
            font: bold 14px;
            min-width: 10em;
            padding: 6px;
            color: #b1b1b1;
        }
        QPushButton:pressed {
            background-color: #454545;
            border-style: inset;
        }
        QPushButton:disabled {
            background-color: #353535;
            color: #808080;
        }
        QTextEdit {
            background-color: #454545;
            color: #b1b1b1;
        }
        QLineEdit {
            background-color: #454545;
            color: #b1b1b1;
        }
        QLabel {
            color: #b1b1b1;
        }
    """)


def main():
    app = QApplication(sys.argv)
    set_dark_mode(app=app)
    screen = app.primaryScreen()
    rect = screen.availableGeometry()
    window = View()
    # Set window size to be a percentage of the screen size
    window.resize(int(rect.width() * 0.5), int(rect.height() * 0.5))  # 50% of screen size

    model = Model()
    view = window
    controller = Controller(model, view)

    window.show()
    app.exec()


if __name__ == "__main__":
    main()
