from openpyxl import *
from openpyxl.styles import  Alignment, Border, PatternFill, Side

import Help_functions
import Trida_oddil
import Trida_dite
import numpy as np

def get_min_and_max_year_of_birth(seznam_oddilu: Trida_oddil.Seznam_oddilu):
	min_year = 10000
	max_year = 0

	for oddil in seznam_oddilu.seznam_oddilu:
		for dite in oddil.seznam_deti:
			if int(dite.r) > max_year:
				max_year = int(dite.r)
			if int(dite.r) < min_year:
				min_year = int(dite.r)
	return min_year, max_year


def init_kids_in_year_numbers(min_year, max_year):
	result = {}
	while min_year <= max_year:
		result[min_year] = 0
		min_year += 1
	return result


# works only up to AZ column
def get_cell_index(column, row_as_string):
	if column < 91:
		return chr(column) + row_as_string  # create current cell index eg. A10
	else:
		column -= 26
		return "A" + chr(column) + row_as_string


def save_into_column(ws, row, column, kids_numbers: dict, celkovy_pocet_deti, average_age, cislo_oddilu, gender: str):
	# create top row
	cell_idx = get_cell_index(column, str(row - 1))
	cell = ws[cell_idx]
	cell.value = str(cislo_oddilu) + gender

	for key, value in kids_numbers.items():
		if value != 0:
			ws[get_cell_index(column, str(row))].value = value
			ws[get_cell_index(column, str(row))].alignment = Alignment(horizontal='center', vertical='center')
		row += 2

	# save total kids count and age average
	ws[get_cell_index(column, str(row))].value = celkovy_pocet_deti
	ws[get_cell_index(column, str(row))].alignment = Alignment(horizontal='center', vertical='center')
	ws[get_cell_index(column, str(row + 1))].value = average_age


def get_number_of_codes(oddil: Trida_oddil.Oddil, boys: bool) -> int:
	count = 0
	for kid in oddil.seznam_deti:
		if boys and kid.is_boy() and kid.pocet_kamaradu() != 0:
			count += 1
		if not boys and kid.is_girl() and kid.pocet_kamaradu() != 0:
			count += 1
	return  count



def save_boys_girls_counts(ws, row: int, column: int, oddil: Trida_oddil.Oddil, girls_numbers: dict, boys_numbers: dict):
	age_sum_boys = 0
	age_sum_girls = 0
	for dite in oddil.seznam_deti:
		if dite.is_boy():
			boys_numbers[int(dite.r)] += 1
			age_sum_boys += dite.age
		else:
			girls_numbers[int(dite.r)] += 1
			age_sum_girls += dite.age

	pocet_holek = len(oddil.seznam_deti) - oddil.pocet_kluku
	age_avg_boys = age_sum_boys / oddil.pocet_kluku
	age_avg_girls = age_sum_girls / pocet_holek

	save_into_column(ws, row, column, girls_numbers, pocet_holek, age_avg_girls, oddil.cislo_oddilu, "D")
	column += 1
	save_into_column(ws, row, column, boys_numbers, oddil.pocet_kluku, age_avg_boys, oddil.cislo_oddilu, "H")

	# save the overall age average in merged cell
	column -= 1
	row_for_average_age = row + len(girls_numbers) * 2 + 3
	ws.merge_cells(get_cell_index(column, str(row_for_average_age)) + ":" + get_cell_index(column + 1, str(row_for_average_age)))
	ws[get_cell_index(column, str(row_for_average_age))].value = oddil.avg_age()

	# save number of kids with codes above the age average
	ws[get_cell_index(column, str(row_for_average_age - 1))].value = get_number_of_codes(oddil, boys=False)
	ws[get_cell_index(column, str(row_for_average_age - 1))].alignment = Alignment(horizontal='center', vertical='center')
	ws[get_cell_index(column + 1, str(row_for_average_age - 1))].value = get_number_of_codes(oddil, boys=True)
	ws[get_cell_index(column + 1, str(row_for_average_age - 1))].alignment = Alignment(horizontal='center', vertical='center')

	# fill empty column with black color
	column += 2
	ws.column_dimensions[chr(column)].width = 2
	for i in range(row_for_average_age):
		ws[get_cell_index(column, str(row + i - 1))].fill = PatternFill(bgColor="00000000", fill_type = "solid")


def save_years(ws, row, column, girls_numbers: dict):
	for key in girls_numbers:
		cell = ws[get_cell_index(column, str(row))]
		cell.value = key
		cell.alignment = Alignment(horizontal='center', vertical='center')
		row += 2

	ws[get_cell_index(column, str(row))].value = "pocty"
	row += 1
	ws[get_cell_index(column, str(row))].value = "vek. prumer"
	row += 1
	ws[get_cell_index(column, str(row))].value = "# kodu"
	row += 1
	ws[get_cell_index(column, str(row))].value = "vek. prumer"


def create_horizontal_line(ws, row):
	column = 65
	while column < 91:
		ws[get_cell_index(column, str(row))].fill = PatternFill(bgColor="00000000", fill_type = "solid")
		column += 1


def create_report(seznam_oddilu: Trida_oddil.Seznam_oddilu, file_name: str, list_name: str):
	# init new work book
	wb = Workbook()
	ws = wb.active
	column = 65
	row = 2
	# get range of years in which kids were born
	min_year, max_year = get_min_and_max_year_of_birth(seznam_oddilu)

	# initialize dictionaries where number of kids born for each year will be stored
	girls_numbers = init_kids_in_year_numbers(min_year, max_year)
	boys_numbers = init_kids_in_year_numbers(min_year, max_year)

	# save years in wb
	save_years(ws, row, column, girls_numbers)

	# for each oddil save number of kids for each year
	column += 1
	for oddil in seznam_oddilu.seznam_oddilu:
		# create new line if end of columns occur "Z"
		if column > 88:
			column = 65
			row += 2 * len(girls_numbers) + 4
			create_horizontal_line(ws, row)
			row += 2
			save_years(ws, row, column, girls_numbers)
			column += 1
		save_boys_girls_counts(ws, row, column, oddil, girls_numbers, boys_numbers)
		column += 3
		girls_numbers = init_kids_in_year_numbers(min_year, max_year)
		boys_numbers = init_kids_in_year_numbers(min_year, max_year)


	wb.save(Help_functions.get_out_path(file_name, f"_{list_name}_report.xlsx"))




