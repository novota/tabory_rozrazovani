import enums
import Trida_dite
import Help_functions
import report_creator
import math
import typing


class Seznam_oddilu:

	def __init__(self, seznam_oddilu=None):
		if seznam_oddilu is None:
			self.seznam_oddilu = []
		else:
			self.seznam_oddilu = seznam_oddilu
		self.rozrazeni_not_possible = False

	def __iadd__(self, other):
		assert isinstance(other, Oddil)
		self.seznam_oddilu.append(other)
		return self

	def __str__(self):
		result = 'Mame nasledujici oddily: \n'
		for element in self.seznam_oddilu:
			result += str(element)
		return result

	def inicializace_oddilu(self, kapacita, pocet, cislo_oddilu):
		while pocet > 0:
			self.seznam_oddilu.append(Oddil(cislo_oddilu, kapacita))
			pocet -= 1
			cislo_oddilu += 1
		return self

	def clear(self):
		self.seznam_oddilu = []
		return self

	# seradi oddily podle poctu deti v nich od nejmene po nejvice
	def seradit_oddily_vzestupne(self):
		index = 1
		while index < len(self.seznam_oddilu):
			j = index - 1
			while j >= 0:
				if self.seznam_oddilu[j + 1].obsazenost() < self.seznam_oddilu[j].obsazenost():
					temp = self.seznam_oddilu[j]
					self.seznam_oddilu[j] = self.seznam_oddilu[j + 1]
					self.seznam_oddilu[j + 1] = temp
					j -= 1
				else:
					break
			index += 1
		return self

	def zaradit_deti_do_oddilu(self, seznam_deti, margin, pocet_kluku_na_oddil=None, aktualni_index_oddil=0):
		friends_collisions = 0
		nr_ooddily_tried = 0  # in case kid with friends doesnt fit in oddil due to low capacity, if kid doesnt fit anywhare quit and repoort it
		while not len(seznam_deti.seznam_deti) == 0:
			maximum_kluku = -1
			oddil = self.seznam_oddilu[aktualni_index_oddil]  # aktualni oddil

			# no more kids without oddil in the list
			if not seznam_deti.find_no_oddil_kid():
				return self

			if pocet_kluku_na_oddil is not None:  # this means we are currently adding boys
				if len(pocet_kluku_na_oddil) == 0:
					assert Help_functions.neexistuje_dite_bez_oddilu(seznam_deti.seznam_deti)
					break

				else:
					maximum_kluku = max(pocet_kluku_na_oddil)

			kid, duvod = Help_functions.find_suitable_kid(seznam_deti, oddil, margin, maximum_kluku, force_delete_in_seznam_deti_navic=friends_collisions >= len(self.seznam_oddilu))

			if duvod == enums.VysledekPridani.OK:
				friends_collisions = 0
				nr_ooddily_tried = 0
				Help_functions.add_kid_to_oddil(oddil, kid)
				seznam_deti.delete_kid_with_friends(kid)  # delete from seznam_deti

				if pocet_kluku_na_oddil is not None:  # this means we are currently adding boys
					if oddil.pocet_kluku == maximum_kluku:
						pocet_kluku_na_oddil.remove(maximum_kluku)

			elif duvod == enums.VysledekPridani.KAMARAD_COLIDES_WITH_SEZNAM_DETI_NAVIC:
				friends_collisions += 1

			elif duvod == enums.VysledekPridani.MALA_KAPACITA:
				nr_ooddily_tried += 1

			# kid with friends does not fit anywhere
			if nr_ooddily_tried == len(self.seznam_oddilu):
				self.rozrazeni_not_possible = True
				return self

			aktualni_index_oddil = Help_functions.update_index_oddily(aktualni_index_oddil, len(self.seznam_oddilu))

		return self

	def naplnit_oddily_na_kapitana_varianta(self, seznam_deti, margin_g, margin_b):
		avg_age = seznam_deti.prumerny_vek()

		# oddelit kluky a holky
		seznam_g = seznam_deti.seznam_holek(with_friends=False, friends_are_important=False)
		seznam_g_friends = seznam_deti.seznam_holek(with_friends=True)
		seznam_g_no_friends = seznam_deti.seznam_holek(with_friends=False)
		seznam_b = seznam_deti.seznam_kluku(with_friends=False, friends_are_important=False)
		seznam_b_friends = seznam_deti.seznam_kluku(with_friends=True)
		seznam_b_no_friends = seznam_deti.seznam_kluku(with_friends=False)
		# seradit od nejstarsiho po nejmladsi
		seznam_g.seradit_sestupne()
		seznam_g_friends.seradit_sestupne()
		seznam_g_no_friends.seradit_sestupne()
		seznam_b.seradit_sestupne()
		seznam_b_friends.seradit_sestupne()
		seznam_b_no_friends.seradit_sestupne()
		# spocitej kolik kluku muze mit maximalne jeden oddil
		pocet_kluku_na_oddil = Help_functions.vytvor_pocty_kluku_na_oddil(len(seznam_b.seznam_deti), len(self.seznam_oddilu))
		# nejdriv zarad kluky
		self.zaradit_deti_do_oddilu(seznam_b_friends, margin_b, pocet_kluku_na_oddil=pocet_kluku_na_oddil)
		self.seznam_oddilu.sort(key=lambda x: x.cislo_oddilu, reverse=True)
		self.zaradit_deti_do_oddilu(seznam_b_no_friends, margin_b, pocet_kluku_na_oddil=pocet_kluku_na_oddil)
		# seradit oddily podle nejnizsiho veku po nejvyssi
		# self.seznam_oddilu.sort(key=lambda x: x.avg_age())
		# zarad holky
		self.seznam_oddilu.sort(key=lambda x: x.cislo_oddilu, reverse=True)
		self.zaradit_deti_do_oddilu(seznam_g_friends, margin_g)
		self.seznam_oddilu.sort(key=lambda x: x.cislo_oddilu, reverse=False)
		self.zaradit_deti_do_oddilu(seznam_g_no_friends, margin_g)
		return self

	def naplnit_oddily_na_kapitana_nejdriv_kody(self, seznam_deti, margin_g, margin_b):
		avg_age = seznam_deti.prumerny_vek()

		# oddelit kluky a holky
		seznam_g = seznam_deti.seznam_holek(with_friends=False, friends_are_important=False)
		seznam_g_friends = seznam_deti.seznam_holek(with_friends=True)
		seznam_g_no_friends = seznam_deti.seznam_holek(with_friends=False)
		seznam_b = seznam_deti.seznam_kluku(with_friends=False, friends_are_important=False)
		seznam_b_friends = seznam_deti.seznam_kluku(with_friends=True)
		seznam_b_no_friends = seznam_deti.seznam_kluku(with_friends=False)
		# seradit od nejstarsiho po nejmladsi
		seznam_g.seradit_sestupne()
		seznam_g_friends.seradit_sestupne()
		seznam_g_no_friends.seradit_sestupne()
		seznam_b.seradit_sestupne()
		seznam_b_friends.seradit_sestupne()
		seznam_b_no_friends.seradit_sestupne()
		# spocitej kolik kluku muze mit maximalne jeden oddil
		pocet_kluku_na_oddil = Help_functions.vytvor_pocty_kluku_na_oddil(len(seznam_b.seznam_deti), len(self.seznam_oddilu))
		# nejdriv zarad kluky
		self.zaradit_deti_do_oddilu(seznam_b_friends, margin_b, pocet_kluku_na_oddil=pocet_kluku_na_oddil)
		self.seznam_oddilu.sort(key=lambda x: x.cislo_oddilu, reverse=True)
		self.zaradit_deti_do_oddilu(seznam_g_friends, margin_g)
		# seradit oddily podle nejnizsiho veku po nejvyssi
		# self.seznam_oddilu.sort(key=lambda x: x.avg_age())
		# zarad holky
		self.seznam_oddilu.sort(key=lambda x: x.cislo_oddilu, reverse=True)
		self.zaradit_deti_do_oddilu(seznam_b_no_friends, margin_b, pocet_kluku_na_oddil=pocet_kluku_na_oddil)
		self.seznam_oddilu.sort(key=lambda x: x.cislo_oddilu, reverse=False)
		self.zaradit_deti_do_oddilu(seznam_g_no_friends, margin_g)
		return self

	# get the difference between oddil with lowest ang age and highest avg age
	def get_avg_age_diff(self):
		min_avg = 100
		max_avg = 0
		for oddil in self.seznam_oddilu:
			if oddil.avg_age() > max_avg:
				max_avg = oddil.avg_age()
			if oddil.avg_age() < min_avg:
				min_avg = oddil.avg_age()
		return max_avg - min_avg

	def clear_kids(self):
		for oddil in self.seznam_oddilu:
			oddil.clear_all_kids()
			oddil.seznam_lidi_navic = []
		self.seznam_oddilu.sort(key=lambda x: x.cislo_oddilu)
		return self

	def clear_seznam_lidi_navic(self, koho_vyhodit):
		for oddil in self.seznam_oddilu:
			temp_list = []
			for dite in oddil.seznam_lidi_navic:
				if dite.gender == koho_vyhodit:
					temp_list.append(dite)
			for dite in temp_list:
				oddil.seznam_lidi_navic.remove(dite)
		return self

	def calculate_error_kids_for_each_year(self, average_counts_of_kids_for_each_year: dict):
		error = 0
		min_year, max_year = report_creator.get_min_and_max_year_of_birth(self)
		for oddil in self.seznam_oddilu:
			oddil_counts = report_creator.init_kids_in_year_numbers(min_year, max_year)
			for kid in oddil.seznam_deti:
				oddil_counts[int(kid.r)] += 1
			for key in oddil_counts:
				temp =  math.floor(abs(oddil_counts[key] - math.ceil(average_counts_of_kids_for_each_year[key])))
				error += temp * temp

		return error



class Oddil:

	def __init__(self, cislo_oddilu=-1, kapacita=15):
		self.kapacita = kapacita
		self.pocet_kluku = 0
		self.seznam_deti = []
		self.pocet_chatek = 0
		self.seznam_cisel_chatek = []
		self.cislo_oddilu = cislo_oddilu
		self.pauzirovani = 0  # kdyz si oddil vybere vic jak jedno dite, pricte se mu sem 1 nebo dva aby priste pauziroval
		self.pauzirovani_b = 0  # separatni counter pro kluky a holky
		self.pauzirovani_g = 0
		self.dalsi_narade = True  # pri rozrazovani urcuje jestli se dalsi priradi kluk nebo holka. Kluk = true, holka = false
		self.seznam_lidi_navic = []
		self.kapacita_kluku = 7

	# holek T54,c48
	# kluku T66, C48

	def __iadd__(self, other):
		assert isinstance(other, Trida_dite.Dite)
		if other.gender == "H":
			self.pocet_kluku += 1
		self.seznam_deti.append(other)
		self.kapacita -= 1
		return self

	def __isub__(self, other):
		assert isinstance(other, Trida_dite.Dite)
		self.seznam_deti.remove(other)
		self.kapacita += 1
		if other.gender == "H":
			self.pocet_kluku -= 1
		return self

	def __str__(self):
		result = 'Oddil c. {:d} with capacity {:d} including {:d} boys with average age of {:f} je v {:d} chatkach: \n'.format(
			self.cislo_oddilu, self.kapacita, self.pocet_kluku, self.avg_age(), self.pocet_chatek)
		# spocitat, v kolika chatkach mame ubytovany deti

		for element in self.seznam_deti:
			result += str(element)
		result += "\n"
		return result

	def clear_all_kids(self):
		for dite in self.seznam_deti:
			dite.oddil = -1
			self.pocet_kluku -= dite.gender == "H"
			self.kapacita += 1
		self.seznam_lidi_navic = []
		self.seznam_deti = []
		return self

	def urcit_pocet_chatek(self):
		for dite in self.seznam_deti:
			if dite.chatka is not None and self.seznam_cisel_chatek.count(dite.chatka.cislo_chatky) == 0:
				self.seznam_cisel_chatek.append(dite.chatka.cislo_chatky)
				self.pocet_chatek += 1
		return self

	def delete(self, dite, valueError="None"):
		assert isinstance(dite, Trida_dite.Dite)
		try:
			self.seznam_deti.remove(dite)
		except valueError as v:
			print('tohle dite v seznamu vazne neni!! ', v)
		return self

	def avg_age(self):
		summ = 0
		count = 0
		for element in self.seznam_deti:
			summ += element.age
			count += 1
		if count == 0:
			return -1
		else:
			return summ / count
