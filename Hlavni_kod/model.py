import datetime
import traceback

import numpy as np
import pandas as pd
import report_creator
import Trida_dite
import Trida_oddil

from enums import VysledekRozrazeni
from Help_functions import check_single_codes, convert_string_to_month_and_day, get_data_from_column, verify_friend_codes_quantity, \
    get_odd_count, get_datetime, all_kids_have_oddil, add_oddily_to_excel
from openpyxl import *
from typing import Any, Tuple


class ExcelError(Exception):
    pass


class Model:
    excel_file_path: str = ""
    oddily_count: int = 0
    oddily_first_number: int = 0  # the first oddil will be assigned this number
    max_kids_oddil: int = 0
    odd_1: int = 0
    odd_2: int = 0
    sheet_names: list
    year_camp_is_taking_place: int = 0
    month_camp_is_taking_place: int = 0
    day_camp_is_taking_place: int = 0
    fried_codes_counts_are_ok: bool = False
    seznam_oddilu = Trida_oddil.Seznam_oddilu()
    seznam_deti = None

    def __init__(self):
        self.current_excel_sheet_name = None
        self.sheet_names = []
        self.excel_wb = None
        self.excel_ws = None

        self.seznam_deti_in_numpy = None

    def create_oddily(self) -> Tuple[VysledekRozrazeni, Any]:
        try:
            self._inicializace_seznam_oddilu()
        except Exception as e:
            return VysledekRozrazeni.INICIALIZACE_SEZNAMU_ODDILU_FAILED, e
        try:
            self._inicializace_seznamu_deti()
        except Exception as e:
            return VysledekRozrazeni.INICIALIZACE_SEZNAMU_DETI_FAILED, e

        try:
            min_error = self._search_for_best_margins()
        except Exception as e:
            return VysledekRozrazeni.SEARCH_FOR_BEST_MARGINS_FAILED, e

        if min_error[1] == -1:
            return VysledekRozrazeni.NO_POSSIBLE_MARGINS_FOUND, Exception()

        print(f"best margin_g is {min_error[1]}, margin_b is {min_error[2]} min error is {min_error[0]}")

        try:
            self._rozrad_dle_nejlepsiho_marginu(min_error=min_error)
        except Exception as e:
            return VysledekRozrazeni.ROZRAD_DLE_NEJLEPSHO_MARGINU_FAILED, e

        try:
            success = self._write_results_to_excel()
        except Exception as e:
            return VysledekRozrazeni.WRITE_RESULTS_TO_EXCEL_FAILED, e
        if not success:
            return VysledekRozrazeni.WRITE_RESULTS_TO_EXCEL_FAILED, f"Nenalezen sloupec 'O' ani 'Odd' pro zapsani vysledku!"

        try:
            self._write_report()
        except Exception as e:
            return VysledekRozrazeni.WRITE_REPORT_FAILED, e

        finally:
            print(self.seznam_oddilu)
            self.seznam_oddilu.clear()
            print(f"executed")

        return VysledekRozrazeni.OK, Exception()

    def _inicializace_seznam_oddilu(self):
        self.seznam_oddilu.inicializace_oddilu(self.max_kids_oddil, self.odd_2, self.oddily_first_number)
        self.seznam_oddilu.inicializace_oddilu(self.max_kids_oddil - 1, self.odd_1, self.oddily_first_number + self.odd_2)
        _ = self.seznam_oddilu.seznam_oddilu[0]  # iff array has not been initialized yet, this yields error

    def _inicializace_seznamu_deti(self):
        tabor_date = get_datetime(str(self.day_camp_is_taking_place), str(self.month_camp_is_taking_place), str(self.year_camp_is_taking_place))
        offset = datetime.date.today() - tabor_date.date()
        offset = offset.days.real
        self.seznam_deti = Trida_dite.Seznam_deti().inicializace_seznamu_deti(self.seznam_deti_in_numpy, offset)

    def _search_for_best_margins(self):
        average_counts_of_kids_for_each_year = self.seznam_deti.get_average_counts_of_kids_for_each_year(len(self.seznam_oddilu.seznam_oddilu))
        min_error = (100000, -1, -1, "normalni")  # (min_error, margin_g, margin_b)
        for margin_g in np.arange(0.4, 1.4, 0.05):  # 0.4 az 1.6
            for margin_b in np.arange(0.4, 1.4, 0.05):
                varianta = "normalni"
                self.seznam_oddilu.naplnit_oddily_na_kapitana_varianta(self.seznam_deti, margin_g, margin_b)

                if self.seznam_oddilu.rozrazeni_not_possible:
                    varianta = "nejdrive_kody"
                    self.seznam_oddilu.rozrazeni_not_possible = False
                    self.seznam_oddilu.clear_kids()
                    self.seznam_oddilu.naplnit_oddily_na_kapitana_nejdriv_kody(self.seznam_deti, margin_g, margin_b)

                if not all_kids_have_oddil(self.seznam_deti):
                    print("not a valid rozrazeni")
                    self.seznam_oddilu.clear_kids()
                    continue

                error = self.seznam_oddilu.calculate_error_kids_for_each_year(average_counts_of_kids_for_each_year)
                if error < min_error[0]:
                    min_error = (error, margin_g, margin_b, varianta)

                self.seznam_oddilu.clear_kids()
        return min_error

    def _rozrad_dle_nejlepsiho_marginu(self, min_error: Tuple[float, float, float, str]):
        if min_error[3] == "normalni":
            self.seznam_oddilu.naplnit_oddily_na_kapitana_varianta(self.seznam_deti, min_error[1], min_error[2])
        else:
            self.seznam_oddilu.naplnit_oddily_na_kapitana_nejdriv_kody(self.seznam_deti, min_error[1], min_error[2])

        for oddil in self.seznam_oddilu.seznam_oddilu:
            oddil.seznam_deti.sort(key=lambda x: x.age, reverse=True)
        self.seznam_oddilu.seznam_oddilu.sort(key=lambda x: x.cislo_oddilu)

    def _write_results_to_excel(self) -> bool:
        a1 = pd.DataFrame(self.seznam_deti.vystup_2D_pro_excel())
        odd = a1.loc[:, 0]
        odd = pd.DataFrame({'seznam': odd.values})
        # for testing purpose create a array with ages of kids and add it to a column in output
        vek = []
        for di in self.seznam_deti.seznam_deti:
            vek.append(di.age)
        success = add_oddily_to_excel(odd, self.excel_file_path, self.excel_wb, self.excel_ws, vek)
        return success

    def _write_report(self):
        report_creator.create_report(self.seznam_oddilu, self.excel_file_path, self.current_excel_sheet_name)

    def load_excel_file(self) -> bool:
        try:
            self.excel_wb = load_workbook(filename=self.excel_file_path)
            self.sheet_names = self.excel_wb.sheetnames
            self.reset_previous_excel_sheet_data()
            return True
        except:
            return False

    def reset_previous_excel_sheet_data(self):
        self.seznam_deti = None
        self.seznam_deti_in_numpy = None
        self.current_excel_sheet_name = None
        self.fried_codes_counts_are_ok = False

    def set_excel_sheet_name(self, excel_sheet_name: str):
        self.current_excel_sheet_name = excel_sheet_name
        self.excel_ws = self.excel_wb[self.current_excel_sheet_name]

    def read_excel_columns(self) -> Tuple[bool, str]:
        columns_needed = ['Jméno', 'R', 'M', 'D', 'S', 'Kód']
        vysledny_seznam = []
        names = get_data_from_column(self.excel_ws, columns_needed[0])
        if names is None:
            return False, f"List neobsahuje sloupce {columns_needed}."
        vysledny_seznam.append(names)
        for idx in range(1, len(columns_needed)):
            current_column = get_data_from_column(self.excel_ws, columns_needed[idx], length=len(names))
            if current_column is None:
                return False, columns_needed[idx]
            # control data, except of Kod, no -1 are allowed. that may later fuck up the create oddily process
            if columns_needed[idx] !=  'Kód' and not self._column_data_is_valid(current_column):
                return False, f"Sloupec {columns_needed[idx]} obsahuje prazdna pole. Vsechna pole u vsech deti musi byt vyplnena!"
            vysledny_seznam.append(current_column)
        self.seznam_deti_in_numpy = np.array(vysledny_seznam).T
        return True, ""

    def _column_data_is_valid(self, a_list) -> bool:
        for value in a_list:
            if str(value) == "-1":
                return False
        return True

    def get_total_children_count(self) -> int:
        rows, *_ = self.seznam_deti_in_numpy.shape
        return rows

    def get_codes_that_appear_once(self) -> list:
        result = []
        singl_kody = check_single_codes(self.seznam_deti_in_numpy)
        if len(singl_kody) > 0:
            for index, code in enumerate(singl_kody):
                text = 'kód ' + str(int(code)) + ' je tu jen jednou'
                result.append(text)
        return result

    def get_children_count_in_oddily(self) -> Tuple:
        # odd_1 is the number of oddily with capacity max_kids_oddil. odd_2 is the number of oddily with capacity max_kids_oddil - 1
        rows, *_ = self.seznam_deti_in_numpy.shape  # how many kids are there in total
        self.odd_1, self.odd_2, self.max_kids_oddil = get_odd_count(rows, self.oddily_count)
        return self.odd_1, self.odd_2, self.max_kids_oddil

    def get_codes_that_appear_more_then_three_times(self):
        result = []
        friends_code, kolikrat = verify_friend_codes_quantity(self.seznam_deti_in_numpy[:, 5])
        if len(friends_code) > 0:
            for index, code in enumerate(friends_code):
                text = 'kód ' + str(int(code)) + ' je tu ' + str(kolikrat[index]) + 'x'
                result.append(text)
        return result

    def set_month_day_from_text(self, text):
        self.day_camp_is_taking_place, self.month_camp_is_taking_place = convert_string_to_month_and_day(input=text)

    def is_data_ready(self) -> bool:
        return bool(self.excel_file_path) and bool(self.oddily_count) and self.oddily_first_number > 0 and bool(self.sheet_names) \
               and bool(self.year_camp_is_taking_place) and bool(self.month_camp_is_taking_place) and bool(self.day_camp_is_taking_place) \
               and self.seznam_deti_in_numpy is not None and self.fried_codes_counts_are_ok

    def reset_input_fields_data(self):
        self.oddily_count = 0
        self.oddily_first_number = 0
        self.year_camp_is_taking_place = 0
