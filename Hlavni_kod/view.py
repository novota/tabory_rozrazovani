
from PyQt6.QtGui import QStandardItemModel, QStandardItem
from PyQt6.QtWidgets import QMainWindow, QWidget, QVBoxLayout, QHBoxLayout,\
    QPushButton, QLineEdit, QGridLayout, QListWidget, QScrollArea, QFileDialog, QLabel, QListView, QSizePolicy, QApplication
from PyQt6.QtCore import Qt
from typing import Callable, List, Tuple


class View(QMainWindow):
    def __init__(self):
        super().__init__()

        # listeners
        self._date_change_listener = None  # when date when the came takes place changes
        self._create_oddily_listener = None
        self._open_file_listener = None
        self._oddily_count_listener = None
        self._oddily_first_number_listener = None
        self._year_listener = None
        self._excel_sheet_selected_listener = None

        self.setWindowTitle("Rozrazovani Do Oddilu")

        # Create the buttons and input fields
        self._button_open_file = QPushButton("Nahraj soubor")
        self._button_create_oddily = QPushButton("Vytvor oddily")
        self._button_exit_program = QPushButton("Zavri program")
        self._button_exit_program.clicked.connect(lambda _: QApplication.quit())

        # button policies
        self._button_open_file.setSizePolicy(QSizePolicy.Policy.Preferred, QSizePolicy.Policy.Expanding)
        self._button_create_oddily.setSizePolicy(QSizePolicy.Policy.Preferred, QSizePolicy.Policy.Expanding)
        self.set_enable_create_oddily_button(flag=False)  # disable until all data has been entered

        # set button events
        self._button_open_file.clicked.connect(lambda _: self._notify_listener(self._open_file_listener))
        self._button_create_oddily.clicked.connect(lambda _: self._notify_listener(self._create_oddily_listener))

        # inputs
        self._input_max_oddily_count = QLineEdit()
        self._input_max_oddily_count.setPlaceholderText("Celkovy pocet oddilu...")
        self._input_oddily_first_number = QLineEdit()
        self._input_oddily_first_number.setPlaceholderText("Prvni oddil ma cislo...")
        self._input_year = QLineEdit()
        self._input_year.setPlaceholderText("Rok konani tabora")

        self.list_of_dates = QListWidget()
        self.list_of_dates.addItems(['4.7.', '18.7.', '25.7.', '1.8.', '8.8.', '15.8.', '22.8.'])
        self.list_of_dates.currentRowChanged.connect(lambda row: self._notify_listener(self._date_change_listener, row))

        # connect input filed changes to callbacks
        self._input_max_oddily_count.textChanged.connect(lambda text: self._notify_listener(self._oddily_count_listener, text))
        self._input_oddily_first_number.textChanged.connect(lambda text: self._notify_listener(self._oddily_first_number_listener, text))
        self._input_year.textChanged.connect(lambda text: self._notify_listener(self._year_listener, text))

        # Create the QScrollArea, add the QListWidget and set the orientation
        self._choose_date_scroll = QScrollArea()
        self._choose_date_scroll.setWidget(self.list_of_dates)
        self._choose_date_scroll.setWidgetResizable(True)
        self._choose_date_scroll.setHorizontalScrollBarPolicy(Qt.ScrollBarPolicy.ScrollBarAsNeeded)

        # shows all excel sheet lists and one of them can be selected
        self._excel_sheet_names = QListView(self)
        self._excel_sheet_names_model = QStandardItemModel(self._excel_sheet_names)
        self._excel_sheet_names.setModel(self._excel_sheet_names_model)
        self._excel_sheet_names.clicked.connect(lambda q_model_index: self._notify_listener(self._excel_sheet_selected_listener,  q_model_index.data(Qt.ItemDataRole.DisplayRole)))
        # excel label
        self.total_children_count = QLabel("0")
        self.total_children_count.setAlignment(Qt.AlignmentFlag.AlignCenter)
        # list of codes that are once or more then 3 times in the excel sheet
        self._list_of_problematic_codes = QListWidget()
        self._problematic_codes = QScrollArea()
        self._problematic_codes.setWidget(self._list_of_problematic_codes)
        self._problematic_codes.setWidgetResizable(True)
        self._problematic_codes.setHorizontalScrollBarPolicy(Qt.ScrollBarPolicy.ScrollBarAsNeeded)

        # Create the left layout
        self._left_layout = QVBoxLayout()
        self._left_layout.addWidget(self._button_exit_program)
        self._left_layout.addWidget(self._button_open_file)
        self._left_layout.addWidget(self._button_create_oddily)

        # Create the right layout
        self._right_layout = QGridLayout()
        self._right_layout.addWidget(self._input_max_oddily_count, 0, 0)
        self._right_layout.addWidget(self._input_oddily_first_number, 1, 0)
        self._right_layout.addWidget(self._input_year, 2, 0)
        self._right_layout.addWidget(QLabel("Datum konani tabora"), 3, 0)
        self._right_layout.addWidget(self._choose_date_scroll, 4, 0, 2, 1)
        self._right_layout.addWidget(self._excel_sheet_names, 0, 1, 2, 2)
        self._right_layout.addWidget(QLabel("Pocet pionyru v seznamu:"), 2, 1, 1, 1)
        self._right_layout.addWidget(self.total_children_count, 2, 2, 1, 1)
        self._right_layout.addWidget(QLabel("Problematicke kody"), 3, 1, 1, 1)
        self._right_layout.addWidget(self._problematic_codes, 4, 1, 2, 2)

        # Combine the two layouts
        self._main_layout = QHBoxLayout()
        self._main_layout.addLayout(self._left_layout, 3)
        self._main_layout.addLayout(self._right_layout, 7)

        # create a label and add it to the bottom of the main layout
        self._bottom_label = QLabel("This is a bottom label")
        self._bottom_label.setAlignment(Qt.AlignmentFlag.AlignCenter)

        # create a new vertical layout, add the main layout and the label to it
        self._vertical_layout = QVBoxLayout()
        self._vertical_layout.addLayout(self._main_layout)
        self._vertical_layout.addWidget(self._bottom_label)

        # Create a QWidget and set the main layout
        _central_widget = QWidget()
        _central_widget.setLayout(self._vertical_layout)
        self.setCentralWidget(_central_widget)

    def force_qui_update(self):
        QApplication.processEvents()  # Force GUI update

    # set listeners
    def set__date_change_listener(self, listener: Callable):
        self._date_change_listener = listener

    def set_open_file_listener(self, listener: Callable):
        self._open_file_listener = listener

    def set_create_oddily_listener(self, listener: Callable):
        self._create_oddily_listener = listener

    def set_oddily_count_listener(self, listener: Callable):
        self._oddily_count_listener = listener

    def set_oddily_first_number_listener(self, listener: Callable):
        self._oddily_first_number_listener = listener

    def set_year_listener(self, listener: Callable):
        self._year_listener = listener

    def set_excel_sheet_selected_listener(self, listener: Callable):
        self._excel_sheet_selected_listener = listener

    # notify listeners
    @staticmethod
    def _notify_listener(listener: Callable, *args):
        if listener is not None:
            listener(*args)

    # setters
    def set_bottom_label(self, text: str, color: Tuple[int, int, int] = (240, 240, 240)):
        self._bottom_label.setText(text)
        self._bottom_label.setStyleSheet(
            f"color: rgb{color}")

        # setters

    def set_total_children_count_label(self, text: str, color: Tuple[int, int, int] = (240, 240, 240)):
        self.total_children_count.setText(text)
        self.total_children_count.setStyleSheet(
            f"color: rgb{color}")

    def set_excel_sheet_names(self, sheet_names: List[str]):
        self._excel_sheet_names_model.clear()
        for name in sheet_names:
            item = QStandardItem(name)
            self._excel_sheet_names_model.appendRow(item)

    def set_list_of_problematic_codes(self, problematic_codes: List[str]):
        self._list_of_problematic_codes.clear()
        self._list_of_problematic_codes.addItems(problematic_codes)

    def set_enable_create_oddily_button(self, flag: bool):
        self._button_create_oddily.setEnabled(flag)

    def open_file_dialog(self) -> str:
        file_dialog = QFileDialog()
        file_name = file_dialog.getOpenFileName(self, 'Open file', '', "Excel files (*.xlsx);;All Files (*)")
        if file_name[0]:
            self._bottom_label.setText(f"Soubor byl uspesne nahran: {file_name[0]}")
            return file_name[0]
        else:
            self._bottom_label.setText("Soubor nevybran")
            return ""

    def load_excel_sheet_names(self):
        self._excel_sheet_names_model.clear()

    def reset_problematic_codes(self):
        self._list_of_problematic_codes.clear()

    def reset_input_fields(self):
        self._input_year.clear()
        self._input_max_oddily_count.clear()
        self._input_oddily_first_number.clear()
