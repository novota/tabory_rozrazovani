
from PyQt6.QtCore import QModelIndex

from enums import VysledekRozrazeni
from model import Model
from view import View


class Controller:

    def __init__(self, model: Model, view: View):

        self.model = model
        self.view = view

        self.set_listeners()

    def open_file(self):
        excel_file_path = self.view.open_file_dialog()
        if not excel_file_path:
            return
        self.model.excel_file_path = excel_file_path
        # excel loaded successfully
        if self.model.load_excel_file():
            self.view.set_excel_sheet_names(sheet_names=self.model.sheet_names)
            # self.reset_user_input_data()
            self.view.reset_problematic_codes()
            # self.view.set_total_children_count_label(text="0")
            self.view.set_bottom_label(text="Soubor byl uspesne nahran")
            self.update_create_oddily_button()
        # excel did not load successfully
        else:
            self.view.set_bottom_label(text="Nastala chyba pri načítání souboru. Zkontroluj názvy sloupců. Musí být: Jméno, R, M, D, S, Kód")

    def create_oddily(self):
        self.view.set_bottom_label(text="probíhá hledání ideálního rozložení dětí do oddílů...")
        self.view.force_qui_update()
        result, exception = self.model.create_oddily()
        if result == VysledekRozrazeni.OK:
            self.view.set_bottom_label(text="Rozdeleni deti do oddilu bylo uspesne", color=(20, 255, 20))
            return
        text = f"Rozrazeni se nepovedlo: {exception}, {result}"
        self.view.set_bottom_label(text=text, color=(255, 20, 20))

    def excel_sheet_selected(self, excel_sheet_name: str):
        self.model.set_excel_sheet_name(excel_sheet_name=excel_sheet_name)
        self.view.set_bottom_label(text=f"Vybral jsi list {excel_sheet_name}")
        result, failed_message = self.model.read_excel_columns()
        if not result:
            self.view.set_bottom_label(text=f"Nastala chyba pri načítání sloupcu: {failed_message}", color=(255, 20, 20))
            self.view.set_total_children_count_label(text=str(0))
            self.model.reset_previous_excel_sheet_data()
            self.view.reset_problematic_codes()
            self.update_create_oddily_button()
            return
        total_children_count = self.model.get_total_children_count()
        self.view.set_total_children_count_label(text=str(total_children_count))
        problematic_codes = []
        self.model.fried_codes_counts_are_ok = True
        problematic_codes.extend(self.model.get_codes_that_appear_more_then_three_times())
        if problematic_codes:
            self.view.set_bottom_label(text="Prover seznam Problematickych kodu. Kod nesmi byt vicekrat nez 3x!!", color=(255, 20, 20))
            self.model.fried_codes_counts_are_ok = False
        problematic_codes.extend(self.model.get_codes_that_appear_once())
        self.view.set_list_of_problematic_codes(problematic_codes=problematic_codes)
        self.update_create_oddily_button()

    def oddily_count_changed(self, text: str):
        number = self.__convert_text_to_number(string_to_num=text, input_field_name="Celkovy pocet oddilu")
        self.model.oddily_count = number
        self.update_create_oddily_button()

    def oddily_first_number_changed(self, text: str):
        number = self.__convert_text_to_number(string_to_num=text, input_field_name="Cislo prvniho oddilu")
        self.model.oddily_first_number = number
        self.update_create_oddily_button()

    def year_changed(self, text: str):
        number = self.__convert_text_to_number(string_to_num=text, input_field_name="Rok konani tabora")
        self.model.year_camp_is_taking_place = number
        self.update_create_oddily_button()

    def reset_user_input_data(self):
        self.view.reset_input_fields()
        self.model.reset_input_fields_data()

    def camp_date_changed(self, row):
        item = self.view.list_of_dates.item(row)
        self.model.set_month_day_from_text(text=item.text())
        self.view.set_bottom_label(text=f"Datum konani tabora nastaveno na {self.model.day_camp_is_taking_place}. {self.model.month_camp_is_taking_place}")
        self.update_create_oddily_button()

    def update_create_oddily_button(self):
        print(f"{self.model.excel_file_path=} {self.model.oddily_count=} {self.model.oddily_first_number=} {self.model.sheet_names=} {self.model.year_camp_is_taking_place=}"
              f"{self.model.month_camp_is_taking_place=} {self.model.day_camp_is_taking_place=}")
        is_data_ready = self.model.is_data_ready()
        self.view.set_enable_create_oddily_button(flag=is_data_ready)
        if is_data_ready:
            self._on_data_is_ready()

    def _on_data_is_ready(self):
        odd_1, odd_2, max_kids_oddil = self.model.get_children_count_in_oddily()
        if odd_1 < 0 or odd_2 < 0:
            self.view.set_bottom_label(text="Vyslo zaporne mnozstvi deti na oddil! Vyslo: " + str(odd_2) + ' po ' + str(max_kids_oddil) + ' dětech a ' + str(odd_1) + ' po ' + str(max_kids_oddil - 1) + ' dětech', color=(255, 10, 10))
        else:
            self.view.set_bottom_label(
                text="Vyslo: " + str(odd_2) + ' po ' + str(max_kids_oddil) + ' dětech a ' + str(odd_1) + ' po ' + str(max_kids_oddil - 1) + ' dětech',
                color=(10, 255, 10))

    def __convert_text_to_number(self, string_to_num: str, input_field_name: str) -> int:
        if not string_to_num:
            self.view.set_bottom_label(text=f"Nastav {input_field_name}")
            return 0
        try:
            number = int(string_to_num)
        except ValueError:
            self.view.set_bottom_label(text=f"{input_field_name} musi byt cislo!", color=(255, 0, 0))
            return 0
        self.view.set_bottom_label(text=f"{input_field_name} nastaven na: {number}")
        return number

    def set_listeners(self):
        self.view.set__date_change_listener(listener=self.camp_date_changed)
        self.view.set_open_file_listener(listener=self.open_file)
        self.view.set_create_oddily_listener(listener=self.create_oddily)
        self.view.set_oddily_count_listener(listener=self.oddily_count_changed)
        self.view.set_oddily_first_number_listener(listener=self.oddily_first_number_changed)
        self.view.set_year_listener(listener=self.year_changed)
        self.view.set_excel_sheet_selected_listener(listener=self.excel_sheet_selected)
